#include "Maze.hpp"
#include "Direction.hpp"

int Maze::getNeighborCell(int cell, int direction) const {
	switch (direction) {
	case Direction::LEFT:
		if (cell % m_width != 0) {
			return cell - 1;
		}
		break;

	case Direction::UP:
		if (cell / m_width != 0) {
			return cell - m_width;
		}
		break;

	case Direction::RIGHT:
		if ((cell + 1) % m_width != 0) {
			return cell + 1;
		}
		break;

	case Direction::DOWN:
		if (cell / m_width - m_width + 1 != 0) {
			return cell + m_width;
		}
		break;

	default:
		break;
	}

	return -1;
}

int Maze::countOpenWalls(void) const {
	int nbOpenWalls(0);
	for (int c = 0; c < m_width * m_height; ++ c) {
		for (int d = 0; d < Direction::COUNT; ++ d) {
			if (hasWall(c, d)) {
				++ nbOpenWalls;
			}
		}
	}
	return nbOpenWalls;
}

std::unique_ptr<Tree<Couple>> Maze::generateTree(int xRoot, int yRoot, int direction) const {
	Couple c(xRoot, yRoot);
	std::unique_ptr<Tree<Couple>> tree(new Tree<Couple>(c));

	int x, y;
	for (int d = 0; d < Direction::COUNT; ++ d) {
		if (d != direction && !hasWall(xRoot, yRoot, d)) {
			Direction::getNeighborCell((x = xRoot), (y = yRoot), d);
			tree->addChild(generateTree(x, y, Direction::getOpposed(d)));
		}
	}

	return std::move(tree);
}

void Maze::print(std::ostream& stream) const {
	// Display the top wall.
	stream << " ";
	for (int x = 0; x < m_width; ++ x) {
		stream << "_ ";
	}
	stream << std::endl;

	// Display the body of the maze.
	for (int y = 0; y < m_height; ++ y) {
		stream << '|';
		for (int x = 0; x < m_width; ++ x) {
			stream << (hasWall(x, y, Direction::DOWN) ? '_' : ' ');
			stream << (hasWall(x, y, Direction::RIGHT) ? '|' : ' ');
		}
		stream << std::endl;
	}
}

#include "2DVisualizer/Maze_2dVisualizer.hpp"

#include "Representations/MazeByWalls.hpp"
#include "Representations/MazeByCells.hpp"

#include "Generator/KruskalGenerator.hpp"
#include "Generator/AldousBroderGenerator.hpp"
#include "Generator/EllerGenerator.hpp"
#include "Generator/UniformGenerator.hpp"
#include "Generator/LooperGenerator.hpp"
#include "Generator/PrimGenerator.hpp"

#include "ExhaustiveGeneration/PerfectGeneration.hpp"
#include "HardnessRater/HardnessRater.hpp"
#include "PropertyChecker/PropertyChecker.hpp"

#include "Solver/MazeBacktrackSolver.hpp"
#include "Solver/MazeWallFollowerSolver.hpp"
#include "Solver/DEndFillingSolver.hpp"

#include <cstdlib>

#define mainSoutenance

#ifdef mainSoutenance
#include <SFML/System.hpp>
#include "3DVisualizer/Window.hpp"
#include "3DVisualizer/Scene.hpp"
#include <chrono>

static bool display3D(const Maze &maze) {
		_3DVisualizer::Window window("Maze3D", 640, 480);
		if (!_3DVisualizer::Scene::initializeOpenGL()) {
			return false;
		}

		_3DVisualizer::Scene scene(640.f/480.f, maze);
		if (!scene.initialize()) {
			return false;
		}

		sf::Time time(sf::milliseconds(100));
		while (window.isOpen()) {
			window.pollEvent(scene);
			window.renderScene(scene);
			sf::sleep(time);
		}
		return true;
}

int main(int argc, char *argv[]) {
	int w, h;
	if (argc == 3) {
		w = atoi(argv[1]);
		h = atoi(argv[2]);
	}
	else {
		w = h = 10;
	}

	MazeByCells maze(w, h);
	std::unique_ptr<MazeGenerator> generator;
	std::unique_ptr<MazeSolver> solver;
	MazeSolution solution;
	std::unique_ptr<Maze_2dVisualizer> visualizer;
	bool disp3D = false;

	std::cout << " :: Générateur de labyrinthes ::" << std::endl;

	std::cout << "Premièrement, appuyez sur une des touches suivantes pour choisir l'algorithme de génération." << std::endl;
	std::cout << "[A] : Aldous-Broder" << std::endl;
	std::cout << "[K] : Kruskal" << std::endl;
	std::cout << "[P] : Prim" << std::endl;
	std::cout << "[E] : Eller (non fonctionnel)" << std::endl;
	std::cout << "[L] : Génération à circuits" << std::endl;
	std::cout << "[A] : Génération uniforme" << std::endl;

	std::cout << "Ensuite, appuyez sur une des touches suivantes pour choisir l'algorithme de résolution." << std::endl;
	std::cout << "[I] : Pas de résolution" << std::endl;
	std::cout << "[B] : Recursive backtracking" << std::endl;
	std::cout << "[M] : Wall Follower" << std::endl;
	std::cout << "[D] : Dead-End filling solver" << std::endl;


	sf::Time time(sf::milliseconds(100));
	while(true) {
		bool generationDone = false;
		while (!generationDone) {
			bool algoChoosed = false;
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
				generator.reset(new AldousBroderGenerator());
				algoChoosed = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::K)) {
				generator.reset(new KruskalGenerator());
				algoChoosed = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::P)) {
				generator.reset(new PrimGenerator());
				algoChoosed = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
				generator.reset(new EllerGenerator());
				algoChoosed = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::L)) {
				generator.reset(new LooperGenerator());
				algoChoosed = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::U)) {
				generator.reset(new UniformGenerator());
				algoChoosed = true;
			}
			if (algoChoosed) {
				std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
				generator->generate(maze);
				std::chrono::duration<double> time = std::chrono::system_clock::now() - start;

				generationDone = true;
				std::cout << "Difficulté du Labyrinthe : " << HardnessRater::evaluate(maze) << std::endl;
				std::cout << "Labyrinthe Parfait? " << PropertyChecker::isPerfectMaze(maze) << std::endl;
				std::cout << "Temps de génération: " << time.count() << "s." << std::endl;
			}
			sf::sleep(time);
		}
		bool solveDone = false;
		bool wantSolve = true;
		while (!solveDone) {
			bool algoChoosed = false;
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::I)) {
				wantSolve = false;
				solveDone = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::B)) {
				solver.reset(new MazeBacktrackSolver());
				algoChoosed = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::M)) {
				solver.reset(new MazeWallFollowerSolver());
				algoChoosed = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
				solver.reset(new DEndFillingSolver());
				algoChoosed = true;
			}
			if (algoChoosed) {
				std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
				solution = solver->solve(maze);
				std::chrono::duration<double> time = std::chrono::system_clock::now() - start;
				std::cout << "Temps de résolution: " << time.count() << "s." << std::endl;
				solveDone = true;
			}
			sf::sleep(time);
		}

		bool chooseDisplay = false;
		std::cout << "Affichage 3D? (O/N)" << std::endl;
		while (!chooseDisplay) {
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::O)) {
				chooseDisplay = true;
				disp3D = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::N)) {
				chooseDisplay = true;
			}
			sf::sleep(time);
		}

		if (!disp3D) {
			if (!wantSolve || !solver->hasSolved()) {
				visualizer.reset(new Maze_2dVisualizer(maze));
			}
			else {
				visualizer.reset(new Maze_2dVisualizer(maze, solution));
			}
			visualizer->mainLoop();
		}

		else {
			if (!display3D(maze)) {
				std::cerr << "Cannot display in 3D." << std::endl;
			}
		}

		bool chooseContinue = false, finished;
		std::cout << "Continuer [O/N]" << std::endl;
		while (!chooseContinue) {
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::O)) {
				chooseContinue = true;
				finished = false;
				break;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::N)) {
				chooseContinue = true;
				finished = true;
				break;
			}
			sf::sleep(time);
		}

		if (finished) {
			break;
		}

		sf::sleep(time);
	}
	return EXIT_SUCCESS;
}

#else

#ifndef main3D
#include <chrono>
int main(int argc, char *argv[]) {
	int w, h;
	if (argc == 3) {
		w = atoi(argv[1]);
		h = atoi(argv[2]);
	}
	else {
		w = h = 10;
	}

	MazeByCells maze1(w,h);
	MazeByWalls maze2(w,h);
	KruskalGenerator generatorK;
	PrimGenerator generatorP;
	AldousBroderGenerator generatorA;
	MazeBacktrackSolver solver;

	std::unique_ptr<Maze_2dVisualizer> visualizer;

	std::cout << "Dimensions of the Maze : ( " << w << ", " << h << " )" << std::endl;

	// Kruskal Generation
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
	generatorA.generate(maze1);
	std::chrono::duration<double> time = std::chrono::system_clock::now() - start;
	std::cout << "Kruskal Generation (MazeByCells) : " << time.count() << "s." << std::endl;
/*
	start = std::chrono::system_clock::now();
	generatorK.generate(maze2);
	time = std::chrono::system_clock::now() - start;
	std::cout << "Kruskal Generation (MazeByWalls) : " << time.count() << "s." << std::endl;

	// Prim Generation
	start = std::chrono::system_clock::now();
	generatorP.generate(maze1);
	time = std::chrono::system_clock::now() - start;
	std::cout << "Prim Generation (MazeByCells) : " << time.count() << "s." << std::endl;

	start = std::chrono::system_clock::now();
	generatorP.generate(maze2);
	time = std::chrono::system_clock::now() - start;
	std::cout << "Prim Generation (MazeByWalls) : " << time.count() << "s." << std::endl;

	// Aldous-Broder Generation
	start = std::chrono::system_clock::now();
	generatorA.generate(maze1);
	time = std::chrono::system_clock::now() - start;
	std::cout << "A-B Generation (MazeByCells) : " << time.count() << "s." << std::endl;

	start = std::chrono::system_clock::now();
	generatorA.generate(maze2);
	time = std::chrono::system_clock::now() - start;
	std::cout << "A-B Generation (MazeByWalls) : " << time.count() << "s." << std::endl;

	// Solve by Backtrack
	std::cout << "Trying to find a solution ..." << std::endl;
	start = std::chrono::system_clock::now();
	MazeSolution solution(solver.solve(maze1));
	time = std::chrono::system_clock::now() - start;
	std::cout << "Backtrack solve time : " << time.count() << "s." << std::endl;

	// Solve by Wall follower
	MazeWallFollowerSolver solver2;
	start = std::chrono::system_clock::now();
	MazeSolution solution2(solver2.solve(maze1));
	time = std::chrono::system_clock::now() - start ;
	std::cout << "Wall follower solve time : " << time.count() << "s." << std::endl;
*/
	// Solve by Dead end filling
	MazeBacktrackSolver solver3;
	start = std::chrono::system_clock::now();
	MazeSolution solution3(solver3.solve(maze1));
	time = std::chrono::system_clock::now() - start ;
	std::cout << "Dead end filling solve time : " << time.count() << "s." << std::endl;


	/*if (!solver3.hasSolved()) {
		std::cout << "No solution found. " << std::endl;
		visualizer.reset(new Maze_2dVisualizer(maze1));

	} else {
		visualizer.reset(new Maze_2dVisualizer(maze1, solution3));
	}
	*/
	visualizer.reset(new Maze_2dVisualizer(maze1));

	std::cout << "Difficulty of the maze: " << HardnessRater::evaluate(maze1) << std::endl;

	visualizer->mainLoop();

	return EXIT_SUCCESS;
}

#else

#include <SFML/System.hpp>
#include "3DVisualizer/Window.hpp"
#include "3DVisualizer/Scene.hpp"

int main(int argc, char *argv[]) {
	_3DVisualizer::Window window("Maze3D", 640, 480);

	if (!_3DVisualizer::Scene::initializeOpenGL()) {
		return EXIT_FAILURE;
	}

	int w, h;
	if (argc == 3) {
		w = atoi(argv[1]);
		h = atoi(argv[2]);
	}
	else {
		w = h = 10;
	}
	MazeGeneratorOld maze1(w,h);
	maze1.primGeneration();

	_3DVisualizer::Scene scene(640.f/480.f, maze1);

	if (!scene.initialize()) {
		return EXIT_FAILURE;
	}

	sf::Time time(sf::milliseconds(100));
	while (window.isOpen()) {
		window.pollEvent(scene);
		window.renderScene(scene);

		sf::sleep(time);
	}
}

#endif
#endif

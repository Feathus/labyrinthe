#if OLD

#include "Solver/MazeSolverOld.hpp"
#include "Maze.hpp"
#include "Direction.hpp"
#include "Tree.hpp"

MazeSolverOld::MazeSolverOld(const Maze& maze)
: m_width(maze.getWidth()), m_height(maze.getHeight()), m_cells(maze.getCells())
{
	m_startCell = m_cells;
	m_exitCell = m_cells + (m_width * m_height - 1);
}

MazeSolverOld::~MazeSolverOld() {
	m_startCell = NULL;
	m_exitCell = NULL;
	delete[] m_cells;
}

std::ostream& operator<<(std::ostream& stream, const MazeSolverOld& maze) {
	maze.print(stream);
	return stream;
}

bool MazeSolverOld::hasWallAt(Cell* cell, int direction) const {
	return *cell & (1 << direction);
}

bool MazeSolverOld::hasBorderWall(Cell *cell, int direction) const {
	int index =  cell -  m_cells;
	switch (direction) {
		case Direction::LEFT:
			return index % m_width == 0;
		break;
		case Direction::RIGHT:
			return index % m_width == m_width - 1;
		break;
		case Direction::UP:
			return index / m_width == 0;
		break;
		case Direction::DOWN:
			return index / m_width == m_height - 1;
		break;
		default:
			return false;
	}
}

bool MazeSolverOld::isBorderCell(Cell *cell) const {
	int index = cell - m_cells;
	return index % m_width == 0 || index / m_width == 0 || index % m_width == m_width - 1 || index / m_width == m_height - 1;
}

bool MazeSolverOld::canMove(Cell *cell, int direction) const {
	return !hasWallAt(cell, direction);
}

bool MazeSolverOld::isVisited(Cell *cell) const {
	return *cell & CELL_VISITED;
}

void MazeSolverOld::setVisited(Cell *cell) const {
	*cell |= CELL_VISITED;
}

void MazeSolverOld::unsetVisited(Cell *cell) const {
	*cell &= ~(CELL_VISITED);
}

void MazeSolverOld::getInfosCell(Cell *cell, int *x, int *y, int *index) const {
	*index = cell - m_cells;
	*x = *index % m_width;
	*y = *index / m_width;
}

int MazeSolverOld::getIndex(Cell *cell) const {
	return cell - m_cells;
}

int MazeSolverOld::getX(Cell *cell) const {
	return getIndex(cell) % m_width;
}

int MazeSolverOld::getY(Cell *cell) const {
	return getIndex(cell) / m_width;
}

Cell* MazeSolverOld::getNextCell(Cell *cell, int direction) const {
	if (!hasBorderWall(cell, direction)) {
		switch(direction) {
			case Direction::LEFT:
				return cell - 1;
			case Direction::RIGHT:
				return cell + 1;
			case Direction::UP:
				return cell - m_width;
			case Direction::DOWN:
				return cell + m_width;
			default:
				return NULL;
		}
	}
	return NULL;
}

bool MazeSolverOld::backtrackSolve() {
	return findPath(m_startCell, NULL, Direction::DOWN, m_width * m_height);
}

bool MazeSolverOld::findPath(Cell* currentCell, Cell *prevCell, int dir, int nbCells) {
	if (!currentCell || currentCell < m_cells || currentCell > m_cells + nbCells) {
		return false;
	}
	if (prevCell && !canMove(prevCell, dir)) {
		return false;
	}
	if (currentCell == m_exitCell) {
		setVisited(currentCell);
		return true;
	}
	setVisited(currentCell);
		Cell* newCell;
		newCell = getNextCell(currentCell, Direction::UP);
		if (newCell != prevCell && findPath(newCell, currentCell, Direction::UP, nbCells)) {
			return true;
		}
		newCell = getNextCell(currentCell, Direction::RIGHT);
		if (newCell != prevCell && findPath(newCell, currentCell, Direction::RIGHT, nbCells)) {
			return true;
		}
		newCell = getNextCell(currentCell, Direction::DOWN);
		if (newCell != prevCell && findPath(newCell, currentCell, Direction::DOWN, nbCells)) {
			return true;
		}
		newCell = getNextCell(currentCell, Direction::LEFT);
		if (newCell != prevCell && findPath(newCell, currentCell, Direction::LEFT, nbCells)) {
			return true;
		}
	unsetVisited(currentCell);
	return false;
}

bool MazeSolverOld::wallFollower() {
	return followWalls(m_startCell, Direction::RIGHT);
}

bool MazeSolverOld::followWalls(Cell *cell, int dir) {
	if (cell == m_exitCell) {
		setVisited(m_exitCell);
		return true;
	}
	setVisited(cell);
	for (int d = 0; d < Direction::COUNT; ++ d) {
		// Cannot go back.
		if (d != Direction::getOpposed(dir)) {
			if (canMove(cell, d) && followWalls(getNextCell(cell, d), d)) {
				return true;
			}
		}
	}
	unsetVisited(cell);
	return false;
}

void MazeSolverOld::printCell(Cell *cell, int i, std::ostream &stream) const {
	if (i == 1) {
		if (hasWallAt(cell, Direction::UP)) {
			stream << "+---";
		}
		else {
			stream << "+   ";
		}
		if (hasBorderWall(cell, Direction::RIGHT)) {
			stream << "+" << std::endl;
		}

	}
	else if (i == 2) {
		if (hasWallAt(cell, Direction::LEFT)) {
			stream << "| " << (isVisited(cell)? "* " : "  ");
		}
		else {
			stream << "  " << (isVisited(cell) ? "* " : "  ");
		}
		if (hasBorderWall(cell, Direction::RIGHT)) {
			stream << "|" << std::endl;
		}
	}
}

void MazeSolverOld::printCellInfo(Cell *cell, std::ostream &stream) const {
	int index, x, y;
	getInfosCell(cell, &index, &x, &y);
	stream << " === Cell ===" << std::endl;
	stream << "Index : " << index << std::endl; 
	stream << "x : " << x << std::endl;
	stream << "y : " << y << std::endl;
	if (hasWallAt(cell, Direction::UP)) {
		stream << "Wall Up - ";
	}
	if (hasWallAt(cell, Direction::LEFT)) {
		stream << "Wall Left - ";
	}
	if (hasWallAt(cell, Direction::RIGHT)) {
		stream << "Wall Right -";
	}
	if (hasWallAt(cell, Direction::DOWN)) {
		stream << "Wall Down";
	}
	stream << std::endl;

}

void MazeSolverOld::print(std::ostream &stream) const {
	int nbCells(m_width * m_height);
	Cell *cellsSize(m_cells + nbCells);
	for (auto cell = m_cells; cell < cellsSize; ) {
		auto c = cell;
		for (auto x = 0; x < m_width; x++, cell++) {
			printCell(cell, 1, stream);
		}
		cell = c;
		for (auto x = 0; x < m_width ; x++, cell++) {
			printCell(cell, 2, stream);
		}
	}
	for (auto x = 0; x < m_width; x++) {
		stream << "+---";
	}
	stream << "+" << std::endl;
}

int MazeSolverOld::getPathLength(void) const {
	int length(0);
	auto sCells(m_cells + m_width * m_height);
	for (auto c = m_cells; c < sCells; c++) {
		if (isVisited(c)) {
			length++;
		}
	}
	return length;
}


int MazeSolverOld::evaluateDifficulty(const Maze& maze) {
	MazeSolverOld m(maze);
	m.backtrackSolve();
	int difficulty(0);
	int size = maze.getWidth() * maze.getHeight();
	std::unique_ptr<Tree<Couple>> t = maze.generateTree(0,0);
	int strahlerNumber = t->getStrahlerNumber();
	int lengthPath = m.getPathLength();
	difficulty = strahlerNumber * size + lengthPath;
	return difficulty;
}

const Cell* MazeSolverOld::getCells(void) const {
	return m_cells;
}

int MazeSolverOld::getWidth(void) const {
	return m_width;
}

int MazeSolverOld::getHeight(void) const {
	return m_height;
}

#endif

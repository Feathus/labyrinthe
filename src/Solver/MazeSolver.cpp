#include "Solver/MazeSolver.hpp"
#include "Maze.hpp"

MazeSolver::MazeSolver()
:	m_maze(nullptr), m_entrance(0), m_exit(0), m_status(true)
{
}

MazeSolver::~MazeSolver() {
}

void MazeSolver::initialize(const Maze& maze, int entrance, int exit) {
	m_maze = &maze;
	m_entrance = entrance;
	m_exit = exit < 0 ? maze.getWidth() * maze.getHeight() - 1 : exit;
	m_status = true;
}

bool MazeSolver::hasSolved() {
	return m_status;
}

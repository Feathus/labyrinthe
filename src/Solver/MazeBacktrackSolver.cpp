#include "Solver/MazeBacktrackSolver.hpp"
#include "Maze.hpp"
#include "Direction.hpp"
#include <iostream>

MazeBacktrackSolver::MazeBacktrackSolver()
:	MazeSolver()
{
}

MazeBacktrackSolver::~MazeBacktrackSolver() {
}

MazeSolution MazeBacktrackSolver::solve(const Maze &maze, int entrance, int exit) {
	initialize(maze, entrance, exit);
	return generateSolution();
}

MazeSolution MazeBacktrackSolver::generateSolution() {
	std::vector<Visit> visited(m_maze->getWidth() * m_maze->getHeight());
	std::list<int> stack;
	int direction;

	stack.push_front(m_entrance);
	visited[m_entrance].m_visited = true;

	int current, next;
	while (!stack.empty()) {
		// If we found the exit, return.
		if ((current = stack.front()) == m_exit) {
			MazeSolution solution(m_entrance);
			for (; current != m_entrance; current = m_maze->getNeighborCell(current, Direction::getOpposed(direction))) {
				direction = visited[current].m_direction;
				solution.push_front(direction);
			}

			return solution;
		}

		stack.pop_front();

		// For each possible direction, push the adjacent cells.
		for (direction = 0; direction < Direction::COUNT; ++ direction) {
			if (!m_maze->hasWall(current, direction)) {
				next = m_maze->getNeighborCell(current, direction);

				if (!visited[next].m_visited) {
					stack.push_front(next);
					visited[next].m_visited = true;
					visited[next].m_direction = direction;
				}
			}
		}
	}

	m_status = false;
	return MazeSolution();
}

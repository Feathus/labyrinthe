/*
 * DEndFillingSolver.cpp
 *
 *  Created on: 21 juin 2015
 *      Author: eric
 */

#include "Solver/DEndFillingSolver.hpp"
#include "Direction.hpp"
#include <random>

DEndFillingSolver::DEndFillingSolver()
: MazeSolver(), m_visited(),  m_nbDeadEndCells(0), m_nbIntersectionCells(0)
{

}

DEndFillingSolver::~DEndFillingSolver() {
}


MazeSolution DEndFillingSolver::solve(const Maze &maze, int entrance, int exit) {
	initialize(maze, entrance, exit);
	int size(maze.getWidth() * maze.getHeight());

	// Initialization
	bool noInc = false;
	m_visited.assign(size, (Cell){false, false});
	m_nbIntersectionCells = 0;
	m_nbDeadEndCells = 0;

	for (int cell(0) ; cell < size; cell ++) {
		if (!m_visited[cell].m_condamned) {
			if (isDeadEnd(cell) && cell != m_entrance && cell != m_exit) {
				m_nbDeadEndCells ++;
				m_nbDeadEnds ++;
				int current(cell);
				int direction(Direction::LEFT);
				int neighbors(0);

				while (!isIntersection(current) && current != m_entrance && current != m_exit) {
					m_nbDeadEndCells ++;
					m_visited[current].m_condamned = true;
					neighbors = getAccessibleNeighbors(current);
					if (!neighbors) {
						noInc = true;
						break;
					}
					direction = chooseDirection(neighbors);
					current = m_maze->getNeighborCell(current, direction);
					noInc = false;
				}
				if (!noInc)
					m_nbIntersectionCells ++;
			}
			else {
				m_visited[cell].m_visited = true;
			}
		}
	}
	return generateBestSolution();
}

int DEndFillingSolver::getAccessibleNeighbors(int cell) {
	int neighbors(0);
	for (int direction = 0; direction < Direction::COUNT; direction ++) {
		if (!m_maze->hasWall(cell, direction)) {
			neighbors |= (1 << direction);
		}
	}
	return neighbors;
}

bool DEndFillingSolver::isAccessibleNeighbor(int neighbors, int direction) {
	return neighbors & (1 << direction);
}

int DEndFillingSolver::chooseDirection(int neighbors) {
	std::random_device randomDevice;
	std::mt19937 randomEngine(randomDevice());
	int direction(0);
	do {
		direction = (std::uniform_int_distribution<>(0, Direction::COUNT-1))(randomEngine);
	}
	while(!isAccessibleNeighbor(neighbors, direction));
	return direction;
}

bool DEndFillingSolver::isIntersection(int cell) {
	return ((int) m_maze->hasWall(cell, Direction::LEFT)
			+ (int) m_maze->hasWall(cell, Direction::RIGHT)
			+ (int) m_maze->hasWall(cell, Direction::UP)
			+ (int) m_maze->hasWall(cell, Direction::DOWN)) > 1;
}

bool DEndFillingSolver::isDeadEnd(int cell) {
	return !isIntersection(cell);
}

int DEndFillingSolver::getDeadEndCells(void) const {
	return m_nbDeadEndCells;
}

int DEndFillingSolver::getDeadEnds(void) const {
	return m_nbDeadEnds;
}

MazeSolution DEndFillingSolver::generateBestSolution(void) {
	unsigned int len(999999999), i(0), index(0);
	if (!generateSolutions() || !m_solutions.size()) {
		return MazeSolution();
	}
	for (MazeSolution s: m_solutions) {
		if (s.size() < len) {
			len = s.size();
			index = i;
		}
		i ++;
	}
	return *std::next(m_solutions.begin(), index);
}

//TODO Etablir plusieurs MazeSolution des cellules marquées VISITEES (la n'en fait qu'une)
bool DEndFillingSolver::generateSolutions(void) {
	MazeSolution solution;
	std::vector<bool> visited(m_maze->getWidth() * m_maze->getHeight(), false);
	int cell = m_entrance;
	visited[m_entrance] = true;
	while (cell != m_exit) {
		bool canContinue = false;
		int neighbor(getAccessibleNeighbors(cell));
		for (int direction(0); direction < Direction::COUNT; direction ++) {
			if (isAccessibleNeighbor(neighbor, direction)) {
				int next = m_maze->getNeighborCell(cell, direction);
				if (m_visited[next].m_visited && !visited[next]) {
					visited[next] = true;
					solution.push_back(direction);
					cell = next;
					canContinue = true;
					break;
				}
			}
		}
		if (canContinue) {
			continue;
		}
		return false;
	}
	m_solutions.push_back(solution);
	return true;


}

#include "Solver/MazeWallFollowerSolver.hpp"
#include "Direction.hpp"

MazeWallFollowerSolver::MazeWallFollowerSolver()
:	MazeSolver()
{
}

MazeWallFollowerSolver::~MazeWallFollowerSolver() {
}

MazeSolution MazeWallFollowerSolver::solve(const Maze& maze, int entrance, int exit) {
	initialize(maze, entrance, exit);
	return followWalls();
}

MazeSolution MazeWallFollowerSolver::followWalls() {
	std::vector<Visit> visited(m_maze->getWidth() * m_maze->getHeight());
	int curd(0), nxtd;

	int current(m_entrance), next;
	visited[m_entrance].m_visited = true;

	while (1) {
		// If we found the exit, return.
		if (current == m_exit) {
			MazeSolution solution(m_entrance);
			for (int direction; current != m_entrance; current = m_maze->getNeighborCell(current, Direction::getOpposed(direction))) {
				direction = visited[current].m_direction;
				solution.push_front(direction);
			}

			return solution;
		}

		// Follow the right wall.
		for (nxtd = Direction::getRotation90(curd); true; nxtd = Direction::getRotation90(nxtd)) {
			if (!m_maze->hasWall(current, nxtd)) {
				next = m_maze->getNeighborCell(current, nxtd);
				break;
			}

			// We are trapped in a cell.
			if (nxtd == curd) {
				m_status = false;
				return MazeSolution();
			}
		}

		// If the cell has already been visited in the same direction, or more than four times, we are trapped in a loop of the maze.
		if (visited[next].m_direction == nxtd || visited[next].m_visited > 3) {
			m_status = false;
			return MazeSolution();
		}

		// Otherwise, we can add the cell to the path.
		++ visited[next].m_visited;
		visited[next].m_direction = nxtd;
		curd = nxtd;
	}

	m_status = false;
	return MazeSolution();
}

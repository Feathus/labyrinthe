/*
 * Maze_byCells.cpp
 *
 *  Created on: 9 mai 2015
 *      Author: eric
 */

#include "Representations/MazeByCells.hpp"
#include "Direction.hpp"

#include <iostream>

MazeByCells::MazeByCells(int w, int h)
:	Maze(w, h), m_cells(w * h, LEFT_WALL | TOP_WALL | RIGHT_WALL | BOTTOM_WALL)
{
}


MazeByCells::~MazeByCells() {
}

bool MazeByCells::hasWall(int x, int y, int direction) const {
	return m_cells[getIndex(x,y)] & (1 << direction);
}

bool MazeByCells::hasWall(int c, int direction) const {
	return m_cells[c] & (1 << direction);
}

void MazeByCells::setWall(int x, int y, int direction, bool set) {
	setWall(x + y * m_width, direction, set);
}

void MazeByCells::setWall(int cell, int direction, bool set) {
	if (set) {
		m_cells[cell] |= (1 << direction);
		if ((cell = getNeighborCell(cell, direction)) >= 0) {
			m_cells[cell] |= (1 << Direction::getOpposed(direction));
		}
	} else {
		m_cells[cell] &= ~(1 << direction);
		if ((cell = getNeighborCell(cell, direction)) >= 0) {
			m_cells[cell] &= ~(1 << Direction::getOpposed(direction));
		}
	}
}

std::vector<Cell> MazeByCells::getCells(void) const {
	return m_cells;
}

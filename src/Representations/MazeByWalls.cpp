/*
 * MazeByWalls.cpp
 *
 *  Created on: 14 juin 2015
 *      Author: eric
 */

#include "Representations/MazeByWalls.hpp"
#include "Direction.hpp"
#define WALL_BORDER -1

MazeByWalls::MazeByWalls(int width, int height)
:	Maze(width, height), m_nbHorizontalWalls((height - 1) * width), m_nbWalls((width - 1) * height + m_nbHorizontalWalls),
    m_walls(m_nbWalls, false)
{
}

MazeByWalls::~MazeByWalls() {
}

bool MazeByWalls::hasWall(int x, int y, int direction) const {
	return hasWall(getIndex(x, y), direction);
}

bool MazeByWalls::hasWall(int cell, int direction) const {
	int wall(getWallIndex(cell, direction));
	return wall == WALL_BORDER || m_walls[wall];
}


void MazeByWalls::setWall(int x, int y, int direction, bool set) {
	int wall(getWallIndex(x + y * m_width, direction));
	if (wall != WALL_BORDER) {
		m_walls[wall] = set;
	}
}

void MazeByWalls::setWall(int cell, int direction, bool set) {
	int wall(getWallIndex(cell, direction));
	if (wall != WALL_BORDER) {
		m_walls[wall] = set;
	}
}

int MazeByWalls::getWallIndex(int cell, int direction) const {
	switch (direction) {
		case Direction::LEFT:
			if (cell % m_width) {
				return (m_nbHorizontalWalls - (cell / m_width) - 1) + cell;
			}
		break;
		case Direction::UP:
			if (cell >= m_width) {
				return cell - m_width;
			}
		break;
		case Direction::RIGHT:
			if ((cell + 1) % m_width) {
				return (m_nbHorizontalWalls - (cell / m_width)) + cell;
			}
		break;
		case Direction::DOWN:
			if (cell < m_nbHorizontalWalls) {
				return cell;
			}
		break;
		default:
			break;
	}

	return WALL_BORDER;
}

std::vector<Cell> MazeByWalls::getCells() const {
	int size = m_width * m_height;
	std::vector<Cell> cells(size);
	for (auto i = 0; i < size ; i++) {
		cells[i] = 0;
		for (int j = Direction::LEFT; j < Direction::COUNT; j ++) {
			int wall = getWallIndex(i, j);
			if (wall != WALL_BORDER && m_walls[wall]) {
				cells[i] |= (1 << j);
			}
			else {
				// Adds a wall if the cell is at the border
				int x = i % m_width;
				int y = i / m_width;
				if (x == 0) {
					cells[i] |= (1 << Direction::LEFT);
				}
				if (x == m_width - 1) {
					cells[i] |= (1 << Direction::RIGHT);
				}
				if (y == 0) {
					cells[i] |= (1 << Direction::UP);
				}
				if (y == m_height - 1) {
					cells[i] |= (1 << Direction::DOWN);
				}
			}
		}
	}

	return std::move(cells);
}

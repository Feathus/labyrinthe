/*
 * HardnessRater.cpp
 *
 *  Created on: 20 juin 2015
 *      Author: eric
 */

#include "HardnessRater/HardnessRater.hpp"
#include "Solver/MazeBacktrackSolver.hpp"
#include "Solver/DEndFillingSolver.hpp"
#include "Maze.hpp"
#include <cmath>

int HardnessRater::evaluate(const Maze& maze) {
	int difficulty(0);
	DEndFillingSolver solver;
	MazeSolution s = solver.solve(maze);
	int nbDeadEnds = solver.getDeadEnds();
	int nbCells(maze.getWidth() * maze.getHeight());
	int nbOpenWalls(maze.countOpenWalls());
	int nbWalls((maze.getWidth() - 1) * maze.getHeight() + (maze.getHeight() - 1) * maze.getWidth());
	int nbCloseWalls(nbWalls - nbOpenWalls);

	int riverFactor = ((float) nbDeadEnds / nbCells) * 100;
	int density = ((float) nbCloseWalls / nbWalls) * 100;
	difficulty =  (riverFactor + density) / 2;
	return difficulty;
}

#include "PropertyChecker/PropertyChecker.hpp"
#include "Maze.hpp"
#include "Direction.hpp"

#include <vector>
#include <list>

class Neighbor {
	friend class PropertyChecker;

public:
	Neighbor(int x, int y, int direction)
	:	m_x(x), m_y(y), m_direction(direction) {
	}

	virtual ~Neighbor() {
	}

private:
	int m_x, m_y;
	int m_direction;
};

bool PropertyChecker::isPerfectMaze(const Maze& maze) {
	int nbVisited, x, y;
	std::vector<std::vector<bool>> visitedMap(maze.getHeight(), std::vector<bool>(maze.getWidth(), false));
	std::list<Neighbor> neighbors;

	// Initialization.
	visitedMap[0][0] = true;
	neighbors.emplace_front(0, 0, -1);
	nbVisited = 1;

	while (!neighbors.empty()) {
		// Pop the first neighbor.
		auto cell(neighbors.begin());

		// Add the accessible neighbors.
		for (int d = 0; d < Direction::COUNT; ++ d) {
			if (d != cell->m_direction && !maze.hasWall(cell->m_x, cell->m_y, d)) {
				x = cell->m_x;
				y = cell->m_y;
				Direction::getNeighborCell(x, y, d);

				// If the neighbor has already been visited, the maze is not perfect.
				if (visitedMap[y][x]) {
					return false;
				}

				visitedMap[y][x] = true;
				neighbors.emplace_front(x, y, Direction::getOpposed(d));
				++ nbVisited;
			}
		}

		neighbors.erase(cell);
	}

	// If all the cells has been visited, then the maze is perfect.
	return nbVisited == maze.getWidth() * maze.getHeight();
}


int PropertyChecker::areSimilarMazes(const Maze& maze1, const Maze& maze2) {
	int similarities(HorizontalSymmetry | VerticalSymmetry | Rotation90 | Rotation180 | Rotation270);
	int w1(maze1.getWidth()), w2(maze2.getWidth()), h1(maze1.getHeight()), h2(maze2.getHeight());

	if (w1 != w2 || h1 != h2) {
		similarities &= ~SameDimensions;
	}

	if (w1 != h2 || h1 != w2) {
		similarities &= ~InvertedDimensions;
	}

	for (int y = 0; y < h1; ++ y) {
		for (int x = 0; x < w1; ++ x) {
			if (similarities == 0) {
				return 0;
			}

			if (similarities & SameDimensions) {
				bool left(maze1.hasWall(x, y, Direction::LEFT)), up(maze1.hasWall(x, y, Direction::UP)),
						right(maze1.hasWall(x, y, Direction::RIGHT)), down(maze1.hasWall(x, y, Direction::DOWN));

				// Horizontal symmetry.
				if (up != maze2.hasWall(w2 - x - 1, y, Direction::UP) || down != maze2.hasWall(w2 - x - 1, y, Direction::DOWN)
					|| left != maze2.hasWall(w2 - x - 1, y, Direction::RIGHT) || right != maze2.hasWall(w2 - x - 1, y, Direction::LEFT)) {
					similarities &= ~HorizontalSymmetry;
				}

				// Vertical symmetry.
				if (left != maze2.hasWall(x, h2 - y - 1, Direction::LEFT) || right != maze2.hasWall(x, h2 - y - 1, Direction::RIGHT)
					|| up != maze2.hasWall(x, h2 - y - 1, Direction::DOWN) || down != maze2.hasWall(x, h2 - y - 1, Direction::UP)) {
					similarities &= ~VerticalSymmetry;
				}

				// Rotation 180.
				if (left != maze2.hasWall(w2 - x - 1, h2 - y - 1, Direction::RIGHT) || right != maze2.hasWall(w2 - x  - 1, h2 - y - 1, Direction::LEFT)
					|| up != maze2.hasWall(w2 - x - 1, h2 - y - 1, Direction::DOWN) || down != maze2.hasWall(w2 - x - 1, h2 - y - 1, Direction::UP)) {
					similarities &= ~Rotation270;
				}
			}

			if (similarities & InvertedDimensions) {
				for (int d = 0; d < Direction::COUNT; ++ d) {
					// Rotation 90.
					if (maze1.hasWall(x, y, d) != maze2.hasWall(w2 - y - 1, x, Direction::getRotation90(d))) {
						similarities &= ~Rotation90;
					}

					// Rotation 270.
					if (maze1.hasWall(x, y, d) != maze2.hasWall(y, h2 - x - 1, Direction::getRotation270(d))) {
						similarities &= ~Rotation270;
					}
				}
			}
		}
	}

	return similarities;
}

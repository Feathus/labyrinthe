/*
 * MazeGeneratedByPrim.cpp
 *
 *  Created on: 15 juin 2015
 *      Author: eric
 */

#include "Generator/PrimGenerator.hpp"
#include "Direction.hpp"
#include <list>

#define WALL_BORDER -1

PrimGenerator::PrimGenerator()
: MazeGenerator()
{
}

PrimGenerator::~PrimGenerator() {
}

void PrimGenerator::generate(Maze& maze) {
	// Initialization of the generator.
	initialize(maze);
	std::list<int> breakables;

	// Cells are currently not in the maze.
	std::vector<std::vector<bool>> cells(maze.getHeight(), std::vector<bool>(maze.getWidth(), false));

	// Pick the entrance cell and mark it as part of the maze.
	int x((std::uniform_int_distribution<>(0, maze.getWidth() - 1))(m_generator)), y((std::uniform_int_distribution<>(0, maze.getHeight() - 1))(m_generator));
	cells[y][x] = true;

	// Adds the walls of the cell to the breakable walls.
	addBreakableWalls(breakables, x, y);

	// While there are walls in the list.
	while (!breakables.empty()) {
		// Pick a random wall from the list.
		int wall, x1, x2, y1, y2;
		wall = extractBreakableWall(breakables);
		getAdjacentCells(wall, x1, y1, x2, y2);

		// If the cell on the opposite side is not in the maze yet.
		if (cells[y1][x1]) {
			x = x2;
			y = y2;
		} else {
			x = x1;
			y = y1;
		}

		if (!cells[y][x]) {
			// Make the wall a passage (break).
			m_walls[wall] = false;

			// Mark the cell on the opposite side as part of the maze.
			cells[y][x] = true;

			// Add the neighboring walls of the cell to the wall list.
			addBreakableWalls(breakables, x, y);
		}
	}

	// Update the maze.
	updateMaze();
}

void PrimGenerator::addBreakableWalls(std::list<int>& breakables, int x, int y) const {
	int wall;
	for (int i = Direction::LEFT; i < Direction::COUNT ; ++ i) {
		if ((wall = getWall(x, y, i)) >= 0 && m_walls[wall]) {
			breakables.push_front(wall);
		}
	}
	breakables.unique();
}

int PrimGenerator::extractBreakableWall(std::list<int> &list) const {
	int randomIndex = (std::uniform_int_distribution<int>(0, list.size()-1))(m_generator);
	auto it = std::next(list.begin(), randomIndex);
	int wall(*it);
	list.erase(it);
	return wall;
}

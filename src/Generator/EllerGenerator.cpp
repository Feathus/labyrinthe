/*
 * EllerGenerator.cpp
 *
 *  Created on: 19 juin 2015
 *      Author: eric
 */

//TODO Essayer de le finir
#include "Generator/EllerGenerator.hpp"
#include "Direction.hpp"

EllerGenerator::EllerGenerator()
: MazeGenerator(), m_lineWidth(0)
{
}

EllerGenerator::~EllerGenerator() {
}

void EllerGenerator::generate(Maze &maze) {
	/*
	- Initialize the cells of the first row to each exist in their own set.
	- Now, randomly join adjacent cells, but only if they are not in the same set.
	When joining adjacent cells, merge the cells of both sets into a single set, indicating that all cells
	in both sets are now connected (there is a path that connects any two cells in the set).
	- For each set, randomly create vertical connections downward to the next row.
	Each remaining set must have at least one vertical connection. The cells in the next row thus connected must share the set of the cell above them.
	- Flesh out the next row by putting any remaining cells into their own sets.
	- Repeat until the last row is reached.
	- For the last row, join all adjacent cells that do not share a set, and omit the vertical connections, and you’re done!

	*/

	// Initialization
	const int size (maze.getWidth() * maze.getHeight());
	std::vector<Cell> cells(size, (Cell) {0, true});
	m_lineWidth = maze.getWidth();
	for (int i = 0; i < size; i++)
	{
		cells[i].set = i;
		cells[i].alone = true;
	}
	initialize(maze);

	// Generation
	for (int y = 0; y < maze.getHeight() - 1; y++) {
		generateLine(y, cells);
	}
	generateLastLine(cells);
	updateMaze();
}

void EllerGenerator::initializeLine(int y) {
	int cell;
	for (int x = 0 ; x < m_maze->getWidth() ; x ++) {
		cell = y * m_maze->getWidth() + x;
		m_cellSets.push_back(CellSet(cell));
	}
}

void EllerGenerator::horizontalConnections(int x, int y, std::vector<Cell> &cells) {
	int isBreaking = (std::uniform_int_distribution<>(0,1))(m_generator);
	if (isBreaking) {
		int cell(y * m_maze->getWidth() + x);
		if (!sameSet(cell, Direction::RIGHT, cells)) {
			breakVerticalWall(cell);
			int neighbor = m_maze->getNeighborCell(cell, Direction::RIGHT);
			int set1 = cells[cell].set;
			int set2 = cells[neighbor].set;
			mergeSets(set1, set2, cells);
			cells[cell].alone = false;
			cells[neighbor].alone = false;
		}
	}

}

void EllerGenerator::verticalConnections(int x, int y, std::vector<Cell> &cells) {
	int cell = y * m_maze->getWidth() + x;
	int isBreaking = cells[cell].alone ? 1 : (std::uniform_int_distribution<>(0,1))(m_generator);
	if (isBreaking) {
		breakHorizontalWall(cell);
		int neighbor = m_maze->getNeighborCell(cell, Direction::DOWN);
		int set1 = cells[cell].set;
		int set2 = cells[neighbor].set;
		mergeSets(set1, set2, cells);
		findSet(set1)->m_connect = true;
	}
}

void EllerGenerator::connectIsolatedCells(int cellLimit, std::vector<Cell> &cells) {
	std::list<CellSet>::iterator it;
	for (it = m_cellSets.begin(); it != m_cellSets.end(); ++ it) {
		// Chaque ensemble doit avoir au moins une connexion avec la ligne suivante.
		if (!it->m_connect && it->m_id < cellLimit ) {
			int n((std::uniform_int_distribution<>(0, it->m_cells.size()))(m_generator));
			int i(*std::next(it->m_cells.begin(), n));
			cells[i].alone = true;
			verticalConnections(i % m_maze->getWidth(), i / m_maze->getWidth(), cells);
			cells[i].alone = false;
		}
	}
}

std::list<EllerGenerator::CellSet>::iterator EllerGenerator::findSet(int idSet) {
	std::list<CellSet>::iterator it = m_cellSets.begin();
	for ( ; it != m_cellSets.end(); ++ it) {
		if (it->m_id == idSet) {
			return it;
		}
	}
	return m_cellSets.end();
}


void EllerGenerator::mergeSets(int set1, int set2, std::vector<Cell> &cells) {
	// Merge list of sets.
	std::list<CellSet>::iterator s1 = findSet(set1);
	std::list<CellSet>::iterator s2 = findSet(set2);
	if (s1 == m_cellSets.end() || s2 == m_cellSets.end()) {
		std::cerr << "Cannot merge. Set doesn't exist." << std::endl;
		return;
	}
	s1->m_cells.merge(s2->m_cells);
	m_cellSets.remove(*s2);

 	// Update cell vector.
	for(int cell: s1->m_cells) {
		cells[cell].set = s1->m_id;
	}
}

bool EllerGenerator::sameSet(int cell, int dir, const std::vector<Cell>& cells) const{
	int nextCell(m_maze->getNeighborCell(cell, dir));
	return cells[cell].set == cells[nextCell].set;
}

void EllerGenerator::generateLine(int y, std::vector<Cell> &cells) {
	initializeLine(y);
	for (int x = 0; x < m_maze->getWidth() - 1; x ++) {
		horizontalConnections(x, y, cells);
	}
	for (int x = 0; x < m_maze->getWidth(); x ++) {
		verticalConnections(x, y, cells);
	}
	connectIsolatedCells(y * m_maze->getWidth() + m_maze->getWidth(), cells);
}

void EllerGenerator::generateLastLine(std::vector<Cell> &cells) {
	for (auto it = m_cellSets.begin(); it != m_cellSets.end(); ++ it) {
		for (int x = 0; x < m_maze->getWidth(); x ++) {
			horizontalConnections(x, m_maze->getHeight() - 1, cells);
		}
	}
}

int EllerGenerator::breakVerticalWall(int cell) {
	int wall(getWall(cell, Direction::RIGHT));
	m_walls[wall] = false;
	return wall;
}

int EllerGenerator::breakHorizontalWall(int cell) {
	int wall(getWall(cell, Direction::DOWN));
	m_walls[wall] = false;
	return wall;
}


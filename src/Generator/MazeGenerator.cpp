#include "Generator/MazeGenerator.hpp"
#include "Maze.hpp"
#include "Direction.hpp"

std::mt19937 MazeGenerator::m_generator((std::random_device())());

MazeGenerator::MazeGenerator()
:	m_maze(nullptr), m_walls(), m_nbHorizontalWalls(0)
{
}

MazeGenerator::~MazeGenerator() {
}

void MazeGenerator::initialize(Maze& maze, bool up) {
	m_maze = &maze;
	m_nbHorizontalWalls = maze.getWidth() * (maze.getHeight() - 1);
	m_walls.assign(m_nbHorizontalWalls + maze.getHeight() * (maze.getWidth() - 1), up);
}

void MazeGenerator::getAdjacentCells(int wall, int& c1, int& c2) const {
	if (wall < m_nbHorizontalWalls) {
		c1 = wall;
		c2 = c1 + m_maze->getWidth();
	} else {
		wall -= m_nbHorizontalWalls;
		c1 = wall / m_maze->getHeight() + (wall % m_maze->getHeight()) * m_maze->getWidth();
		c2 = c1 + 1;
	}
}

void MazeGenerator::getAdjacentCells(int wall, int& x1, int& y1, int& x2, int& y2) const {
	if (wall < m_nbHorizontalWalls) {
		x1 = wall % m_maze->getWidth();
		x2 = x1;
		y1 = wall / m_maze->getWidth();
		y2 = y1 + 1;

	} else {
		wall -= m_nbHorizontalWalls;
		y1 = wall % m_maze->getHeight();
		y2 = y1;
		x1 = wall / m_maze->getHeight();
		x2 = x1 + 1;
	}
}

int MazeGenerator::getWall(int cell, int direction) const {
	return getWall(cell % m_maze->getWidth(), cell / m_maze->getWidth(), direction);
}

int MazeGenerator::getWall(int x, int y, int direction) const {
	switch (direction) {
		case Direction::LEFT:
			if (x > 0) {
				return m_nbHorizontalWalls + (x - 1) * m_maze->getHeight() + y;
			}
			break;

		case Direction::UP:
			if (y > 0) {
				return (y - 1) * m_maze->getWidth() + x;
			}
			break;

		case Direction::RIGHT:
			if (x < m_maze->getWidth() - 1) {
				return m_nbHorizontalWalls + x * m_maze->getHeight() + y;
			}
			break;

		case Direction::DOWN:
			if (y < m_maze->getHeight() - 1) {
				return y * m_maze->getWidth() + x;
			}
			break;

		default:
			break;
	}

	return -1;
}

void MazeGenerator::updateMaze() const {
	for (int wall = 0; wall < m_nbHorizontalWalls; ++ wall) {
		// Horizontal walls.
		m_maze->setWall(wall % m_maze->getWidth(), wall / m_maze->getWidth(), Direction::DOWN, m_walls[wall]);
	}

	int nbVerticals(m_walls.size() - m_nbHorizontalWalls);
	for (int wall = 0; wall < nbVerticals; ++ wall) {
		// Vertical walls.
		m_maze->setWall(wall / m_maze->getHeight(), wall % m_maze->getHeight(), Direction::RIGHT, m_walls[wall + m_nbHorizontalWalls]);
	}
}

#include "Generator/AldousBroderGenerator.hpp"
#include "Maze.hpp"
#include "Direction.hpp"

#include <algorithm>
#include <iostream>

#define VISITED_CELL 16

AldousBroderGenerator::AldousBroderGenerator()
: MazeGenerator()
{
}

AldousBroderGenerator::~AldousBroderGenerator() {
}

void AldousBroderGenerator::generate(Maze& maze) {
	m_maze = &maze;
	const int nbCells(maze.getWidth() * maze.getHeight());
	const int width(maze.getWidth());

	// We start with a grid full of walls.
	std::vector<unsigned char> cells(nbCells, (unsigned char) 15);
	int unvisitedCells(nbCells);


	// At start, choose a cell randomly.
	int currentCell = (std::uniform_int_distribution<>(0, nbCells - 1))(m_generator);
	setVisited(&cells[currentCell]);
	unvisitedCells --;

	int xCell = currentCell % width, yCell = currentCell / width;

	// While all vertices are not visited
	while (unvisitedCells > 0) {

		// Choose a connected neighbor of the cell, and travel to it.
		int direction = (std::uniform_int_distribution<>(Direction::LEFT, Direction::COUNT - 1))(m_generator);
		Direction::getNeighborCell(xCell, yCell, direction);
		int neighborCell = yCell * width + xCell;

		// If the cell are out of the maze, remove this neighbor and continue.
		if (!maze.isCellInside(xCell, yCell)) {
			xCell = currentCell % width;
			yCell = currentCell / width;
			continue;
		}

		// If the neighbor has not been visited, break the wall.
		if (!isVisited(cells[neighborCell])) {
			breakWall(&cells[currentCell], direction);
			breakWall(&cells[neighborCell], Direction::getOpposed(direction));
			setVisited(&cells[neighborCell]);
			unvisitedCells --;
		}

		currentCell = neighborCell;
	}

	// Update the maze.
	for (int cell = 0; cell < nbCells; ++ cell) {
		maze.setWall(cell, Direction::DOWN, hasWall(cells[cell], Direction::DOWN));
		maze.setWall(cell, Direction::RIGHT, hasWall(cells[cell], Direction::RIGHT));
	}

	m_maze = nullptr;
}

bool AldousBroderGenerator::hasWall(unsigned char cell, int direction) {
	return cell & (1 << direction);
}

void AldousBroderGenerator::breakWall(unsigned char *cell, int direction) {
	*cell &= ~(1 << direction);
}

void AldousBroderGenerator::setVisited(unsigned char *cell) {
	*cell |= VISITED_CELL;
}

bool AldousBroderGenerator::isVisited(unsigned char cell) {
	return cell & VISITED_CELL;
}

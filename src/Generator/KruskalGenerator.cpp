/*
 * MazeGeneratedByKruskal.cpp
 *
 *  Created on: 15 juin 2015
 *      Author: eric
 */

#include "Generator/KruskalGenerator.hpp"
#include "Direction.hpp"
#include <iostream>
#include <stdio.h>

KruskalGenerator::KruskalGenerator()
: MazeGenerator()
{
}

KruskalGenerator::~KruskalGenerator() {

}

void KruskalGenerator::generate(Maze& maze) {
	//Initialization of the map.
	initialize(maze);

	const int nbWalls(maze.getWidth() * (2 * maze.getHeight() - 1) - maze.getHeight());
	const int size(maze.getWidth() * maze.getHeight());
	std::vector<int> cells(size);
	std::list<CellGroup> groupList;
	std::list<int> breakables;

	// Initialize the cells to their number.
	for (int i = 0; i < size; ++ i) {
		cells[i] = i;
		groupList.push_back(i);
	}

	// All walls are breakables at the beginning.
	for (int i = 0; i < nbWalls; ++ i) {
		breakables.push_back(i);
	}

	while (breakables.size() > 0) {
		// Choose randomly a wall to break.
		int wall(*std::next(breakables.cbegin(), (std::uniform_int_distribution<>(0, breakables.size() - 1))(m_generator)));
		m_walls[wall] = false;

		// Unite the two sections.
		int c1, c2;
		getAdjacentCells(wall, c1, c2);
		uniteCells(cells, cells[c1], cells[c2], groupList, breakables);
	}

	// Update the maze.
	updateMaze();
}

void KruskalGenerator::uniteCells(std::vector<int>& cells, int g1, int g2, std::list<CellGroup>& groupList, std::list<int>& breakables) {
	std::list<CellGroup>::iterator it1, it2;
	if (g1 > g2) {
		std::swap(g1, g2);
	}

	// Remove the walls between them from the breakable walls.
	for (auto wall = breakables.begin(); wall != breakables.end(); ) {
		int c1, c2;
		getAdjacentCells(*wall, c1, c2);

		if (cells[c1] > cells[c2]) {
			std::swap(c1, c2);
		}

		if (cells[c1] == g1 && cells[c2] == g2) {
			wall = breakables.erase(wall);

		} else {
			++ wall;
		}
	}

	// Find the groups the cells belong to.
	for (auto group = groupList.begin(); group != groupList.end(); ++ group) {
		if (group->m_group == g1) {
			it1 = group;
		} else if (group->m_group == g2) {
			it2 = group;
			break;
		}
	}

	// Merge the smallest group into the largest one.
	if (it1->m_cells.size() < it2->m_cells.size()) {
		std::swap(it1, it2);
	}

	for (auto cell: it2->m_cells) {
		cells[cell] = it1->m_group;
		it1->m_cells.push_front(cell);
	}

	groupList.erase(it2);
}



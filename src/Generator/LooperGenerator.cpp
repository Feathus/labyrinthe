#include "Generator/LooperGenerator.hpp"
#include "Maze.hpp"
#include "Direction.hpp"
#include <iostream>

LooperGenerator::LooperGenerator()
:	MazeGenerator()
{
}

LooperGenerator::~LooperGenerator() {
}

void LooperGenerator::generate(Maze& maze) {
	// Initialization.
	initialize(maze);
	std::vector<bool> visited(maze.getWidth() * maze.getHeight(), false);
	std::vector<ConnectionCount> counts(m_walls.size());
	int toVisit(visited.size() - 1);
	int current((std::uniform_int_distribution<>(0, toVisit))(m_generator));
	visited[current] = true;

	// The border and corner walls have less than 6 adjacent walls.
	for (unsigned int wall = 0; wall < m_walls.size(); ++ wall) {
		updateBorderCount(wall, counts);
	}

	int neighbor, direction;
	std::list<Connection> connected;
	// Continue as long as all vertices have not been visited.
	while (toVisit > 0) {
		// Choose a neighbor of the cell.
		do {
			direction = (std::uniform_int_distribution<>(0, Direction::COUNT - 1))(m_generator);
		} while ((neighbor = m_maze->getNeighborCell(current, direction)) < 0);
		int wall(getWall(current, direction));

		// Retrieve the connected walls.
		if (direction == Direction::LEFT || direction == Direction::UP) {
			getConnectedWalls(wall, connected, current, neighbor);
		} else {
			getConnectedWalls(wall, connected, neighbor, current);
		}

		if (!visited[neighbor]) {
			// If the cell has never been visited, break the wall.
			visited[neighbor] = true;
			breakWall(wall, counts, connected);
			-- toVisit;

		} else {
			// Otherwise, check that the wall can be broken.
			if (counts[wall].m_count1 > 0 && counts[wall].m_count2 > 0) {
				if ((std::exponential_distribution<float>(6 - counts[wall].m_count1 - counts[wall].m_count2))(m_generator) > 1.5f) {
					breakWall(wall, counts, connected);
				}
			}
		}

		current = neighbor;
	}

	// Update the maze.
	updateMaze();
}

void LooperGenerator::updateBorderCount(int wall, std::vector<ConnectionCount>& counts) {
	int size;
	if (wall < m_nbHorizontalWalls) {
		size = m_maze->getWidth();
	} else {
		size = m_maze->getHeight();
	}

	if (wall % size == 0) {
		-- counts[wall].m_count1;
	}

	if ((wall + 1) % size == 0) {
		-- counts[wall].m_count2;
	}
}

void LooperGenerator::getConnectedWalls(int wall, std::list<Connection>& connected, int c1, int c2) {
	int nw, size, direction;
	connected.clear();

	if (wall < m_nbHorizontalWalls) {
		size = m_maze->getWidth();
		direction = Direction::LEFT;
	} else {
		size = m_maze->getHeight();
		direction = Direction::UP;
	}

	// Get side walls.
	if (wall % size > 0) {
		connected.emplace_front(wall - 1, false);
	}

	if ((wall + 1) % size > 0) {
		connected.emplace_front(wall + 1, true);
	}

	// Get c1 walls.
	if ((nw = getWall(c1, direction)) >= 0) {
		connected.emplace_front(nw, true);
	}

	if ((nw = getWall(c1, Direction::getOpposed(direction))) >= 0) {
		connected.emplace_front(nw, true);
	}

	// Get c2 walls.
	if ((nw = getWall(c2, direction)) >= 0) {
		connected.emplace_front(nw, false);
	}

	if ((nw = getWall(c2, Direction::getOpposed(direction))) >= 0) {
		connected.emplace_front(nw, false);
	}
}

void LooperGenerator::breakWall(int wall, std::vector<ConnectionCount>& counts, const std::list<Connection>& connected) {
	m_walls[wall] = false;
	for (auto nw: connected) {
		if (nw.m_side && counts[nw.m_wall].m_count1 > 0) {
			-- counts[nw.m_wall].m_count1;
		} else if (counts[nw.m_wall].m_count2 > 0) {
			-- counts[nw.m_wall].m_count2;
		}
	}
}

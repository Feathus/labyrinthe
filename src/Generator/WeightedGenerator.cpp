#include "Generator/WeightedGenerator.hpp"
#include "Maze.hpp"

WeightedGenerator::WeightedGenerator()
: KruskalGenerator()
{
}

WeightedGenerator::~WeightedGenerator() {
}

void WeightedGenerator::generate(Maze& maze) {
	//Initialization of the map.
	initialize(maze);

	const int nbWalls(maze.getWidth() * (2 * maze.getHeight() - 1) - maze.getHeight());
	const int size(maze.getWidth() * maze.getHeight());
	std::vector<int> cells(size);
	std::list<CellGroup> groupList;
	std::list<int> breakables;

	// Initialize the cells to their number.
	for (int i = 0; i < size; ++ i) {
		cells[i] = i;
		groupList.push_back(i);
	}

	// All walls are breakables at the beginning.
	for (int i = 0; i < nbWalls; ++ i) {
		breakables.push_back(i);
	}

	int wall;
	while (breakables.size() > 0) {
		// The algorithm choose either a wall that bound two different sections, or a completely random wall.
		if ((std::uniform_int_distribution<>(0, 1))(m_generator)) {
			wall = *std::next(breakables.cbegin(), (std::uniform_int_distribution<>(0, breakables.size() - 1))(m_generator));
		} else {
			wall = (std::uniform_int_distribution<>(0, m_walls.size() - 1))(m_generator);
		}

		m_walls[wall] = false;

		// Unite the two sections.
		int c1, c2;
		getAdjacentCells(wall, c1, c2);

		if (cells[c1] != cells[c2]) {
			uniteCells(cells, cells[c1], cells[c2], groupList, breakables);
		}
	}

	// Update the maze.
	updateMaze();
}

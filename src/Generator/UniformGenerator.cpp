#include "Generator/UniformGenerator.hpp"
#include "Maze.hpp"

#include <vector>
#include <list>

UniformGenerator::UniformGenerator()
: MazeGenerator()
{
}

UniformGenerator::~UniformGenerator() {
}

void UniformGenerator::generate(Maze& maze) {
	// Initialize the generator.
	initialize(maze);
	int nbWalls(maze.getWidth() * (2 * maze.getHeight() - 1) - maze.getHeight());
	std::list<int> upWalls;

	// Walls to pick.
	for (auto wall = 0; wall < nbWalls; ++ wall) {
		upWalls.push_front(wall);
	}

	// Pick a random number of walls.
	int nbToStand((std::binomial_distribution<>(nbWalls))(m_generator));

	// While current number of walls is greater than the number of walls at the end of generation.
	while (nbWalls > nbToStand) {
		// Pick a random wall from the list.
		int index((std::uniform_int_distribution<>(0, nbWalls - 1))(m_generator));
		auto wall(std::next(upWalls.begin(), index));

		// Break this wall.
		m_walls[*wall] = false;

		// Remove this wall from the list.
		upWalls.erase(wall);
		-- nbWalls;
	}

	// Update the maze.
	updateMaze();
}

#include "Generator/WilsonGenerator.hpp"
#include "Direction.hpp"
#include <algorithm>

#include <iostream>

WilsonGenerator::WilsonGenerator()
: MazeGenerator()
{
}

WilsonGenerator::~WilsonGenerator() {
}

//FIXME Parfois génère des labyrinthes non parfaits (il doit manquer une étape..)
void WilsonGenerator::generate(Maze &maze) {
	initialize(maze, true);
	int nbCells(maze.getWidth() * maze.getHeight());
	std::vector<bool> remainingCells(nbCells, true);
	std::list<int> cellsToPick;
	int nbRemainingCells(nbCells);

	// At start, all cells are not in the UST.
	for (int i (0) ; i < nbCells; i++) {
		cellsToPick.push_back(i);
	}

	// Choose any cell at random.
	int startCell((std::uniform_int_distribution<>(0, nbCells - 1))(m_generator));

	// Add the start cell to the UST
	addCellToUST(remainingCells, nbRemainingCells, startCell);
	cellsToPick.remove(startCell);

	// While all cells are not in the UST
	while (nbRemainingCells > 0) {

		// Select any cell at random that is not already in the UST
		int cellIndex((std::uniform_int_distribution<>(0, cellsToPick.size() - 1))(m_generator));
		auto cell(std::next(cellsToPick.begin(), cellIndex));

		// Perform a random walk until you encounter a cell that is in the UST
		randomWalk(remainingCells, nbRemainingCells, *cell, chooseRandomDirection(*cell));

		// Break the wall at the opposite direction if the selected cell is not a border cell ?


		//Update the list
		updateList(remainingCells, cellsToPick);
	}
	updateMaze();
}

void WilsonGenerator::updateList(const std::vector<bool>& remainingCells, std::list<int>& cellsToPick) {
	for (unsigned int i = 0; i < remainingCells.size(); i++) {
		if (!remainingCells[i]) {
			cellsToPick.remove(i);
		}
	}
}

void WilsonGenerator::addCellToUST(std::vector<bool>& remainingCells, int &nbRemainingCells, int cell) {
	remainingCells[cell] = false;
	nbRemainingCells --;
}

bool WilsonGenerator::isRemainingCell(const std::vector<bool> &remainingCells, int cell) {
	return remainingCells[cell];
}

void WilsonGenerator::randomWalk(std::vector<bool> &remainingCells, int &nbRemainingCells, int startCell, int startDirection) {
	int nextCell(startCell), dir(startDirection), nextDir;
	while (true) {
		// Choose randomly the next direction
		nextDir = (std::uniform_int_distribution<>(Direction::LEFT, Direction::COUNT - 1))(m_generator);
		if (nextDir == Direction::getOpposed(dir) || !isValidDirection(nextCell, nextDir) ) {
			continue;
		}

		// Break the wall between the two cells if the next cell is not in the UST.
		if (isRemainingCell(remainingCells, nextCell)) {
			addCellToUST(remainingCells, nbRemainingCells, nextCell);
			m_walls[getWall(nextCell, nextDir)] = false;
			nextCell = m_maze->getNeighborCell(nextCell, nextDir);
			dir = nextDir;
			continue;
		}

		// Else we finished.
		else {
			m_walls[getWall(nextCell, nextDir)] = false;
			return;
		}
	}
}

int WilsonGenerator::chooseRandomDirection(int cell){
	while (true) {
		int dir((std::uniform_int_distribution<>(Direction::LEFT, Direction::COUNT - 1))(m_generator));
		if (isValidDirection(cell, dir)) {
			return dir;
		}
	}
	return -1;
}

bool WilsonGenerator::isValidDirection(int cell, int dir) {
	int nextCell(m_maze->getNeighborCell(cell, dir));
	if (!m_maze->isCellInside(nextCell % m_maze->getWidth(), nextCell / m_maze->getWidth())) {
		return false;
	}
	return true;
}

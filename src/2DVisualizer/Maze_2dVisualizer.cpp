#include "2DVisualizer/Maze_2dVisualizer.hpp"
#include "Direction.hpp"

#include <algorithm>

#define DEFAULT_W_WINDOW 1024
#define DEFAULT_H_WINDOW 768

void Maze_2dVisualizer::initializeWindow() {
	getIdealDimensions();
	setView();
	m_window.create(sf::VideoMode(m_widthWindow, m_heightWindow), "Maze", sf::Style::Default);
	m_window.setFramerateLimit(30);
}

Maze_2dVisualizer::Maze_2dVisualizer(const Maze& maze)
:	m_maze(&maze), m_solution(nullptr)
{
	initializeWindow();
}

Maze_2dVisualizer::Maze_2dVisualizer(const Maze &maze, const MazeSolution& solution)
:	m_maze(&maze), m_solution(&solution)
{
	initializeWindow();
}

Maze_2dVisualizer::~Maze_2dVisualizer() {
	m_window.close();
}

void Maze_2dVisualizer::getIdealDimensions(void) {
	m_zoneMaze = sf::IntRect(m_border, m_border, m_border + DEFAULT_W_WINDOW - m_wZButtons, DEFAULT_H_WINDOW - 2 * m_border);
	m_sizeCell = std::min(m_zoneMaze.width/m_maze->getWidth(), m_zoneMaze.height/m_maze->getHeight());
	if (m_sizeCell < 2) {
		m_sizeCell = 2;
	}
	m_heightWindow = m_maze->getHeight() * m_sizeCell + 2 * m_border;
	m_widthWindow = m_maze->getWidth() * m_sizeCell + 2 * m_border;
	m_zoneMaze = sf::IntRect(m_border, m_border, m_border + m_widthWindow, m_heightWindow - 2 * m_border);
//	m_widthWindow = m_maze->getWidth() * m_sizeCell + m_border + m_wZButtons;
//	m_zoneMaze = sf::IntRect(m_border, m_border, m_border + m_widthWindow - m_wZButtons, m_heightWindow - 2 * m_border);
}

void Maze_2dVisualizer::displayVerticalWall(int x, int y) {
	sf::Vertex vWall[] = {
		sf::Vertex(sf::Vector2f(m_border + x * m_sizeCell, m_border + y * m_sizeCell), m_colorMaze),
		sf::Vertex(sf::Vector2f(m_border + x * m_sizeCell, m_border + y * m_sizeCell + m_sizeCell), m_colorMaze)
	};
	m_window.draw(vWall, 2, sf::Lines);
}

void Maze_2dVisualizer::displayHorizontalWall(int x, int y) {
	sf::Vertex hWall[] = {
		sf::Vertex(sf::Vector2f(m_border + x * m_sizeCell, m_border + y * m_sizeCell), m_colorMaze),
		sf::Vertex(sf::Vector2f(m_border + x * m_sizeCell + m_sizeCell, m_border + y * m_sizeCell), m_colorMaze)
	};
	m_window.draw(hWall, 2, sf::Lines);
}

void Maze_2dVisualizer::displaySolutionCell(int x, int y) {
	sf::RectangleShape indicator(sf::Vector2f(m_sizeCell , m_sizeCell ));
	sf::Vector2f pos = sf::Vector2f(m_border + x * m_sizeCell, m_border + y * m_sizeCell);
	indicator.setFillColor(sf::Color::Red + sf::Color::Blue);
	indicator.setPosition(pos.x, pos.y);
	m_window.draw(indicator);

}

void Maze_2dVisualizer::displaySolution(void) {
	if (m_solution) {
		int x, y;
		m_maze->getCoordinates(m_solution->getEntrance(), x, y);
		displaySolutionCell(x, y);

		for (int d: *m_solution) {
			Direction::getNeighborCell(x, y, d);
			displaySolutionCell(x, y);
		}
	}
}

void Maze_2dVisualizer::displayMaze(void) {
	for (int x = 0; x < m_maze->getWidth(); ++ x) {
		displayHorizontalWall(x, 0);
	}

	for (int y = 0; y < m_maze->getHeight(); ++ y) {
		displayVerticalWall(0, y);

		for (int x = 0; x < m_maze->getWidth(); ++ x) {
			if (m_maze->hasWall(x, y, Direction::RIGHT)) {
				displayVerticalWall(x + 1, y);
			}

			if (m_maze->hasWall(x, y, Direction::DOWN)) {
				displayHorizontalWall(x, y + 1);
			}
		}
	}
}

void Maze_2dVisualizer::setView(void) {
	float x = m_border / (float) m_widthWindow;
	float y = m_border / (float) m_heightWindow;
	float w = m_zoneMaze.width / (float) m_widthWindow;
	float h = m_zoneMaze.height / (float) m_heightWindow;
	m_view.reset(sf::FloatRect(m_zoneMaze.left - 2, m_zoneMaze.top, m_zoneMaze.width + 2, m_zoneMaze.height + 2));
	m_view.setViewport(sf::FloatRect(x, y, w, h));
}

void Maze_2dVisualizer::zoomIn(void) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		m_view.zoom(0.9);
	}

}

void Maze_2dVisualizer::zoomOut(void) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
		m_view.zoom(1.1);
	}
}

void Maze_2dVisualizer::moveLeft(void) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		m_view.move(-3., 0.);
	}
}

void Maze_2dVisualizer::moveRight(void) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		m_view.move(3., 0.);
	}
}

void Maze_2dVisualizer::moveUp(void) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		m_view.move(0., -3.);
	}
}

void Maze_2dVisualizer::moveDown(void) {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		m_view.move(0., 3.);
	}
}

void Maze_2dVisualizer::mainLoop(void) {
	sf::Time time(sf::milliseconds(100));
	while (m_window.isOpen()) {
		sf::Event event;
		while (m_window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				m_window.close();
				return;
			}
		}

		m_window.clear(m_bgColor);
		m_window.setView(m_view);
		zoomIn();
		zoomOut();
		moveLeft();
		moveRight();
		moveUp();
		moveDown();
		displaySolution();
		displayMaze();
		m_window.setView(m_window.getDefaultView());
		m_window.display();
		sf::sleep(time);
	}
}


#include "3DVisualizer/Wall.hpp"
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace _3DVisualizer {

const glm::vec3 Wall::horizontalScale(1.f, 1.f, 0.2f);
const glm::vec3 Wall::verticalScale(0.2f, 1.f, 1.f);

Wall::Wall(const Cube& cube)
:	Element(cube, 1.f, 0.5f, 0.f)
{
}

Wall::~Wall() {
}

void Wall::draw(int modelLocation, const Properties& properties) const {
	glm::mat4 model(glm::translate(properties.m_position));
	model = glm::scale(model, properties.m_orientation == Orientation::HORIZONTAL ? horizontalScale : verticalScale);

	glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(model));

	Element::draw();
}

} /* namespace _3DVisualizer */

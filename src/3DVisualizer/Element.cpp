#include "3DVisualizer/Cube.hpp"
#include <GL/glew.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cstring>
#include "3DVisualizer/Element.hpp"

namespace _3DVisualizer {

Element::Element(const Cube& cube, float red, float green, float blue)
	: m_cube(cube), m_colorVbo(0), m_vao(0)
{
	int size(cube.getNbVertices() * 3);
	float *colors = new float[size];

	for (float *c = colors, *e = c + size; c < e; ) {
		*(c ++) = red;
		*(c ++) = green;
		*(c ++) = blue;
	}

	generateColorVBO(colors, size);
	generateVAO(3);

	delete[] colors;
}

Element::~Element() {
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_colorVbo);
}


void Element::generateColorVBO(float *colors, int size) {
	glGenBuffers(1, &m_colorVbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_colorVbo);
		glBufferData(GL_ARRAY_BUFFER, size * sizeof(float), colors, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Element::generateVAO(int nbColorComponents) {
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ARRAY_BUFFER, m_cube.getVBO());
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid const*)offsetof(Vertex, position));
		glBindBuffer(GL_ARRAY_BUFFER, m_colorVbo);
			glVertexAttribPointer(1, nbColorComponents, GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}


void Element::draw() const {
	glBindVertexArray(m_vao);
		glDrawArrays(GL_TRIANGLES, 0, m_cube.getNbVertices());
	glBindVertexArray(0);
}

} /* namespace _3DVisualizer */

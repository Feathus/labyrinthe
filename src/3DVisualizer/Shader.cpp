#include "3DVisualizer/Shader.hpp"
#include <iostream>
#include <fstream>
#include <cerrno>
#include <cstring>

namespace _3DVisualizer {

static bool readFile(std::string &dst, const std::string& path) {
	std::string line;

	std::ifstream stream(path);
	if (!stream.is_open()) {
		std::cerr << strerror(errno) << std::endl;
		return false;
	}

	while (getline(stream, line)) {
		dst += line + '\n';
	}

	stream.close();

	return true;
}

static bool checkShaderBuild(GLuint shaderID) {
	GLint status(0);
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE) {
		char *error;
		GLint size(0);
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &size);

		error = new char[size + 1];
		glGetShaderInfoLog(shaderID, size, &size, error);
		error[size] = '\0';

		std::cerr << error << std::endl;
		delete[] error;

		return false;
	}

	return true;
}

static bool buildShader(GLuint &shaderID, const std::string &path, GLenum shaderType) {
	std::string code;
	char const *codeChar;

	shaderID = glCreateShader(shaderType);

	if (!readFile(code, path)) {
		return false;
	}

	codeChar = code.c_str();
	glShaderSource(shaderID, 1, &codeChar, 0);
	glCompileShader(shaderID);

	return checkShaderBuild(shaderID);
}

static bool checkProgramBuild(GLuint programID) {
	GLint status(0);
	glGetProgramiv(programID, GL_LINK_STATUS, &status);

	if (status != GL_TRUE) {
		char *error;
		GLint size(0);
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &size);

		error = new char[size + 1];
		glGetProgramInfoLog(programID, size, &size, error);
		error[size] = '\0';

		std::cerr << error << std::endl;
		delete[] error;

		return false;
	}

	return true;
}


Shader::Shader(const std::string &vertexShader, const std::string &fragmentShader)
	: m_vertexPath(vertexShader), m_fragmentPath(fragmentShader), m_programID(0), m_projection(0), m_view(0), m_model(0)
{
}

Shader::~Shader() {
	glDeleteProgram(m_programID);
}

bool Shader::load() {
	GLuint vertexID, fragmentID;
	if (!buildShader(vertexID, m_vertexPath, GL_VERTEX_SHADER) || !buildShader(fragmentID, m_fragmentPath, GL_FRAGMENT_SHADER)) {
		return false;
	}

	m_programID = glCreateProgram();
	glAttachShader(m_programID, vertexID);
	glAttachShader(m_programID, fragmentID);

	glLinkProgram(m_programID);

	glDetachShader(m_programID, fragmentID);
	glDetachShader(m_programID, vertexID);

	glDeleteShader(fragmentID);
	glDeleteShader(vertexID);

	if (!checkProgramBuild(m_programID)) {
		return false;
	}

	m_projection = glGetUniformLocation(m_programID, "projection");
	m_view = glGetUniformLocation(m_programID, "view");
	m_model = glGetUniformLocation(m_programID, "model");

	return true;
}

void Shader::bind(bool bind) const {
	if (bind) {
		glUseProgram(m_programID);
	} else {
		glUseProgram(0);
	}
}

} /* namespace L3Projet */

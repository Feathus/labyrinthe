#include "3DVisualizer/Window.hpp"
#include "3DVisualizer/Scene.hpp"
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <GL/glew.h>
#include <iostream>
#include <functional>

namespace _3DVisualizer {

Window::Window(const std::string &name, int width, int height)
    : m_window(nullptr)
{
    sf::ContextSettings context;
        context.depthBits = 24;
        context.stencilBits = 0;
        context.antialiasingLevel = 4;
        context.majorVersion = 3;
        context.minorVersion = 3;
    m_window = new sf::Window(sf::VideoMode(width, height), name, sf::Style::Default, context);
}

Window::~Window() {
	delete m_window;
}

bool Window::isOpen() {
	return m_window->isOpen();
}

void Window::renderScene(Scene& scene) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	scene.draw();
	m_window->display();
}

bool Window::pollEvent(Scene& scene) {
	sf::Event event;
	while (m_window->pollEvent(event)) {
		if (event.type == sf::Event::Closed) {
			m_window->close();
			return false;
		}

		if (event.type == sf::Event::KeyPressed) {
			switch (event.key.code) {
			case sf::Keyboard::Key::Down:
				scene.rotateCamera(0.f, 0.1f);
				break;

			case sf::Keyboard::Key::Up:
				scene.rotateCamera(0.f, -0.1f);
				break;

			case sf::Keyboard::Key::Right:
				scene.rotateCamera(0.1f, 0.f);
				break;

			case sf::Keyboard::Key::Left:
				scene.rotateCamera(-0.1f, 0.f);
				break;

			case sf::Keyboard::Key::S:
				scene.zoomCamera(0.1f);
				break;

			case sf::Keyboard::Key::Z:
				scene.zoomCamera(-0.1f);
				break;

			case sf::Keyboard::Key::Q:
				scene.shiftCamera(0.1f);
				break;

			case sf::Keyboard::Key::D:
				scene.shiftCamera(-0.1f);
				break;

			default:
				break;
			}
		}
	}

	return true;
}

void Window::close() {
	m_window->close();
}

double Window::getRatio() {
	sf::Vector2u size(m_window->getSize());
	return ((double) size.x) / size.y;
}

} /* namespace 3DVisualizer */

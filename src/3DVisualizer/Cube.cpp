#include "3DVisualizer/Cube.hpp"

// 12 faces * 3 vertices per face
#define CUBE_SIZE 36

namespace _3DVisualizer {

Cube::Cube()
:	Mesh(CUBE_SIZE)
{
	Vertex *vertices = new Vertex[CUBE_SIZE];

	construct(vertices);
	generateVBO(vertices);

	delete[] vertices;
}

void Cube::construct(Vertex *vertices) const {
	float positions[][3] = {
			{0.5f, -0.5f, 0.5f}, 	{0.5f, -0.5f, -0.5f},	{-0.5f, -0.5f, -0.5f},	{-0.5f, -0.5f, 0.5f},
			{0.5f, 0.5f, 0.5f}, 	{0.5f, 0.5f, -0.5f},	{-0.5f, 0.5f, -0.5f},	{-0.5f, 0.5f, 0.5f}
	};

	float normals[][3] = {
			{0.f, -1.f, 0.f},		{1.f, 0.f, 0.f},		{0.f, 0.f, -1.f},		{-1.f, 0.f, 0.f},
			{0.f, 0.f, 1.f},		{0.f, 1.f, 0.f}
	};

	createFace(vertices, positions[0], normals[0], positions[1], normals[0], positions[3], normals[0]);
	createFace(vertices += 3, positions[1], normals[0], positions[2], normals[0], positions[3], normals[0]);

	createFace(vertices += 3, positions[0], normals[1], positions[1], normals[1], positions[4], normals[1]);
	createFace(vertices += 3, positions[1], normals[1], positions[4], normals[1], positions[5], normals[1]);

	createFace(vertices += 3, positions[1], normals[2], positions[2], normals[2], positions[5], normals[2]);
	createFace(vertices += 3, positions[2], normals[2], positions[6], normals[2], positions[5], normals[2]);

	createFace(vertices += 3, positions[2], normals[3], positions[3], normals[3], positions[6], normals[3]);
	createFace(vertices += 3, positions[3], normals[3], positions[6], normals[3], positions[7], normals[3]);

	createFace(vertices += 3, positions[0], normals[4], positions[3], normals[4], positions[4], normals[4]);
	createFace(vertices += 3, positions[3], normals[4], positions[4], normals[4], positions[7], normals[4]);

	createFace(vertices += 3, positions[4], normals[5], positions[5], normals[5], positions[6], normals[5]);
	createFace(vertices += 3, positions[4], normals[5], positions[6], normals[5], positions[7], normals[5]);
}

} /* namespace _3DVisualizer */

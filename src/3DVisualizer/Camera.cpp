#include "3DVisualizer/Camera.hpp"
#include <glm/gtx/transform.hpp>
#include <iostream>

namespace _3DVisualizer {

static void rotateCamera(glm::vec3 &dst, const glm::vec3 &src, float theta, float phi) {
	glm::vec3 tgt_eye = dst - src;
	float distance = glm::length(tgt_eye);

	// "Normalization" of the angles.
	theta = 2.f * glm::asin(theta / (2.f * distance));
	phi = 2.f * glm::asin(phi / (2.f * distance));

	// Application of the rotations.
	glm::mat4 rotation = glm::translate(src);
	rotation = glm::rotate(rotation, theta, glm::vec3(0.f, 1.f, 0.f));
	rotation = glm::rotate(rotation, phi, glm::cross(tgt_eye / distance, glm::vec3(0.f, 1.f, 0.f)));
	glm::vec4 result = rotation * glm::vec4(tgt_eye, 0.f);

	// Modification of the position of the camera.
	dst = src + glm::vec3(result.x, result.y, result.z);
}


Camera::Camera(glm::vec3 position, glm::vec3 target, double angle, double ratio, double near, double far)
	: m_position(position), m_target(target),
	  m_projection(glm::perspective(angle, ratio, near, far)), m_view(glm::lookAt(m_position, m_target, glm::vec3(0.f, 1.f, 0.f))), m_hasChanged(false)
{
}

Camera::~Camera() {
}

void Camera::changePosition(glm::vec3 position) {
	m_position = position;
	m_hasChanged = true;
}

void Camera::changeTarget(glm::vec3 target) {
	m_target = target;
	m_hasChanged = true;
}

void Camera::move(float theta, float phi) {
	rotateCamera(m_position, m_target, theta, phi);
	m_hasChanged = true;
}

void Camera::reorientate(float theta, float phi) {
	rotateCamera(m_target, m_position, theta, phi);
	m_hasChanged = true;
}

void Camera::zoom(float length) {
	float norm(glm::length(m_target - m_position));
	if (length > norm - 1.f) {
		length = norm - 1.f;
	}

	m_position = m_position + length / norm * (m_target - m_position);
	m_hasChanged = true;
}

void Camera::shift(float length) {
	glm::vec3 tangent(glm::normalize(glm::cross(m_target - m_position, glm::vec3(0.f, 1.f, 0.f))));
	m_position += length * tangent;
	m_target += length * tangent;
	m_hasChanged = true;
}

const glm::mat4& Camera::getView() {
	if (m_hasChanged) {
		m_view = glm::lookAt(m_position, m_target, glm::vec3(0.f, 1.f, 0.f));
		m_hasChanged = false;
	}
	return m_view;
}

} /* namespace _3DVisualizer */

#include "3DVisualizer/Mesh.hpp"
#include <cstring>
#include <iostream>

namespace _3DVisualizer {

Mesh::Mesh(int nbVertices)
	: m_nbVertices(nbVertices), m_vbo(0)
{
}

Mesh::~Mesh() {
	glDeleteBuffers(1, &m_vbo);
}


void Mesh::createVertex(Vertex& vertex, float const position[3], float const normal[3]) {
	memcpy(&vertex.position, position, sizeof(float[3]));
	memcpy(&vertex.normal, normal, sizeof(float[3]));
}

void Mesh::createFace(Vertex face[3], const Vertex& v1, const Vertex& v2, const Vertex& v3) {
	memcpy(face, &v1, sizeof(Vertex));
	memcpy(face + 1, &v2, sizeof(Vertex));
	memcpy(face + 2, &v3, sizeof(Vertex));
}

void Mesh::createFace(Vertex face[3], float const pos1[3], float const norm1[3], float const pos2[3], float const norm2[3],
		float const pos3[3], float const norm3[3]) {
	Vertex v1, v2, v3;
	createVertex(v1, pos1, norm1);
	createVertex(v2, pos2, norm2);
	createVertex(v3, pos3, norm3);

	createFace(face, v1, v2, v3);
}


void Mesh::generateVBO(Vertex const *vertices) {
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBufferData(GL_ARRAY_BUFFER, m_nbVertices * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

} /* namespace _3DVisualizer */

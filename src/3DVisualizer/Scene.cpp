#include "3DVisualizer/Scene.hpp"
#include "Maze.hpp"
#include "Direction.hpp"

#include <GL/glew.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

namespace _3DVisualizer {

bool Scene::initializeOpenGL() {
	static bool isAlreadyInitialized = false;

	if (!isAlreadyInitialized) {
		GLenum status = glewInit();
		if (status != GLEW_OK) {
			std::cerr << "Initialization of GLEW failed: " << glewGetErrorString(status) << std::endl;
			return false;
		}

		isAlreadyInitialized = true;
	}

	glEnable(GL_DEPTH_TEST);

	return true;
}

Scene::Scene(double windowRatio, const Maze& maze)
	: m_shader("data/shader.vert", "data/shader.frag"),
	  m_camera(glm::vec3(maze.getWidth() / 2.f - 0.5f, 30.f, maze.getHeight() + 5.f), glm::vec3(maze.getWidth() / 2.f - 0.5f, 0.f, maze.getHeight() / 2.f - 0.5f), 70.0, windowRatio, 1.0, 100.0),
	  m_cube(), m_wall(m_cube), m_wallProperties(), m_floor(m_cube, 0.8f, 0.f, 0.1f), m_width(maze.getWidth()), m_height(maze.getHeight())
{
	for (int y = 0; y < m_height; ++ y) {
		for (int x = 0; x < m_width; ++ x) {
			if (maze.hasWall(x, y, Direction::LEFT)) {
				m_wallProperties.emplace_back(glm::vec3(x - 0.5f, 0.5f, y), Wall::Orientation::VERTICAL);
			}

			if (maze.hasWall(x, y, Direction::UP)) {
				m_wallProperties.emplace_back(glm::vec3(x, 0.5f, y - 0.5f), Wall::Orientation::HORIZONTAL);
			}
		}
	}

	for (int x = 0; x < m_width; ++ x) {
		m_wallProperties.emplace_back(glm::vec3(x, 0.5f, m_height - 0.5f), Wall::Orientation::HORIZONTAL);
	}

	for (int y = 0; y < m_height; ++ y) {
		m_wallProperties.emplace_back(glm::vec3(m_width - 0.5f, 0.5f, y), Wall::Orientation::VERTICAL);
	}
}

Scene::~Scene() {
}

bool Scene::initialize() {
	if (!m_shader.load()) {
		return false;
	}

	return true;
}

void Scene::draw() {
	glm::mat4 viewMat(m_camera.getView());
	glm::mat4 projectionMat(m_camera.getProjection());
	glm::mat4 floorView(glm::translate(glm::vec3(m_width / 2.f - 0.5f, -0.1f, m_height / 2.f - 0.5f)));
	floorView = glm::scale(floorView, glm::vec3(m_width, 0.2f, m_height));

	m_shader.bind(true);
		glUniformMatrix4fv(m_shader.getProjectionLocation(), 1, GL_FALSE, glm::value_ptr(projectionMat));
		glUniformMatrix4fv(m_shader.getViewLocation(), 1, GL_FALSE, glm::value_ptr(viewMat));

		for (const Wall::Properties& properties: m_wallProperties) {
			m_wall.draw(m_shader.getModelLocation(), properties);
		}

		glUniformMatrix4fv(m_shader.getModelLocation(), 1, GL_FALSE, glm::value_ptr(floorView));
		m_floor.draw();

	m_shader.bind(false);
}

void Scene::rotateCamera(float dtheta, float dphi) {
	m_camera.move(dtheta, dphi);
}

void Scene::zoomCamera(float length) {
	m_camera.zoom(length);
}

void Scene::shiftCamera(float length) {
	m_camera.shift(length);
}

} /* namespace _3DVisualizer */

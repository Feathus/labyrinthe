#include "ExhaustiveGeneration/PerfectGeneration.hpp"
#include "Representations/MazeByCells.hpp"
#include "Direction.hpp"
#include "PropertyChecker/PropertyChecker.hpp"

#include <utility>

int PerfectGeneration::m_width(0), PerfectGeneration::m_height(0);
std::vector<bool> PerfectGeneration::m_walls;
int PerfectGeneration::m_nbHorizontals(0), PerfectGeneration::m_nbVerticals(0);

std::vector<std::unique_ptr<Maze>> PerfectGeneration::generateAll(int width, int height) {
	std::vector<std::unique_ptr<Maze>> mazes;
	unsigned int wall;

	initialize(width, height);

	// Generate all the possible non perfect mazes.
	while (true) {
		std::unique_ptr<Maze> current(createMaze());
		if (PropertyChecker::isPerfectMaze(*current)) {
			// If the generated maze is perfect, add it to the container.
			mazes.push_back(std::move(current));
		}

		// Find the first wall that is down.
		for (wall = 0; wall < m_walls.size() && m_walls[wall]; ++ wall) {
			// Break all the previous ones.
			m_walls[wall] = false;
		}

		// All walls are up. All mazes have been generated.
		if (wall == m_walls.size()) {
			break;
		}

		// Set up the found wall.
		m_walls[wall] = true;
	}

	return std::move(mazes);
}

void PerfectGeneration::initialize(int width, int height) {
	m_width = width;
	m_height = height;
	m_nbHorizontals = width * (height - 1);
	m_nbVerticals = height * (width - 1);
	m_walls.assign(m_nbHorizontals + m_nbVerticals, false);
}

std::unique_ptr<Maze> PerfectGeneration::createMaze() {
	std::unique_ptr<Maze> maze(new MazeByCells(m_width, m_height));

	for (int wall = 0; wall < m_nbHorizontals; ++ wall) {
		// Horizontal walls.
		maze->setWall(wall % m_width, wall / m_width, Direction::DOWN, m_walls[wall]);
	}

	for (int wall = 0; wall < m_nbVerticals; ++ wall) {
		// Vertical walls.
		maze->setWall(wall / m_height, wall % m_height, Direction::RIGHT, m_walls[wall + m_nbHorizontals]);
	}

	return std::move(maze);
}

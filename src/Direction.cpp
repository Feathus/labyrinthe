#include "Direction.hpp"
#include <ostream>

int Direction::getOpposed(int direction) {
	switch (direction) {
	case LEFT:
		return RIGHT;
	case UP:
		return DOWN;
	case RIGHT:
		return LEFT;
	case DOWN:
		return UP;
	default:
		break;
	}

	return -1;
}

void Direction::getNeighborCell(int& x, int& y, int direction) {
	switch (direction) {
	case LEFT:
		-- x;
		break;
	case UP:
		-- y;
		break;
	case RIGHT:
		++ x;
		break;
	case DOWN:
		++ y;
		break;
	}
}

int Direction::getRotation90(int direction) {
	switch (direction) {
	case LEFT:
		return UP;
	case UP:
		return RIGHT;
	case RIGHT:
		return DOWN;
	case DOWN:
		return LEFT;
	default:
		break;
	}
	return -1;
}


int Direction::getRotation270(int direction) {
		switch (direction) {
		case LEFT:
			return DOWN;
		case UP:
			return LEFT;
		case RIGHT:
			return UP;
		case DOWN:
			return RIGHT;
		default:
			break;
		}
		return -1;
	}

void Direction::print(std::ostream &stream, int direction) {
	switch (direction) {
	case LEFT:
		stream << "LEFT" << std::endl;
		break;
	case DOWN:
		stream << "DOWN" << std::endl;
		break;
	case UP:
		stream << "UP" << std::endl;
		break;
	case RIGHT:
		stream << "RIGHT" << std::endl;
		break;
	default:
		stream << "Invalid direction" << std::endl;
	}
}

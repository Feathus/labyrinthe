#version 330

layout(location=0) in vec3 vx_position;
layout(location=1) in vec3 vx_color;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out vec3 color;

void main()
{
	gl_Position = projection * view * model * vec4(vx_position, 1.0);
	color = vx_color;
}

#ifndef EXHAUSTIVEGENERATION_PERFECTGENERATION_HPP_
#define EXHAUSTIVEGENERATION_PERFECTGENERATION_HPP_

#include <vector>
#include <memory>

class Maze;

class PerfectGeneration {
public:
	/**
	 * @brief Generate all the perfect mazes of the given dimension.
	 */
	static std::vector<std::unique_ptr<Maze>> generateAll(int width, int height);

private:
	/**
	 * @brief Initialize the generation members.
	 */
	static void initialize(int width, int height);

	/**
	 * @brief Return the maze generated using m_walls.
	 */
	static std::unique_ptr<Maze> createMaze();

	/**
	 * @brief Dimensions of the mazes currently generated.
	 */
	static int m_width, m_height;

	/**
	 * @brief The walls of the mazes.
	 */
	static std::vector<bool> m_walls;

	/**
	 * @brief The number of walls.
	 */
	static int m_nbHorizontals, m_nbVerticals;
};

#endif /* PERFECTGENERATION_HPP_ */

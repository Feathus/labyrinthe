/*
 * Maze.hpp
 *
 *  Created on: 8 mai 2015
 *      Author: eric
 */

#ifndef INCLUDE_MAZE_HPP_
#define INCLUDE_MAZE_HPP_

#include "Tree.hpp"
#include <fstream>
#include <memory>

/**
 * @brief A cell of the maze is defined as a byte.
 */
typedef unsigned char Cell;

typedef enum {
	LEFT_WALL = 1, TOP_WALL = 2, RIGHT_WALL = 4, BOTTOM_WALL = 8,
	IN_SOLUTION = 16

} CellProperty;

 /**
  * @brief (x,y) couple.
  */
struct Couple {
	/**
	 * @brief (x,y)
	 */
 	int x, y;

 	/**
 	 * @brief Constructs a couple.
 	 */
 	Couple()
 	: x(0), y(0)
 	{
 	}

 	Couple(int x, int y)
 	: x(x), y(y)
	{}

 	Couple(const Couple& c)
 	: x(c.x), y(c.y)
 	{}
 };

 /**
  * @brief Overloading of << for Couple structure.
  */
 std::ostream& operator<<(std::ostream& stream, const Couple& couple);

/**
 * @brief A Maze.
 */
class Maze {
public:
	/**
	 * @brief Constructor.
	 */
	Maze(int width, int height)
	:	m_width(width), m_height(height) {
	}

	/**
	 * @brief Virtual destructor.
	 */
	virtual ~Maze() {
	}

	/**
	 * @brief Get the width of the maze.
	 */
	int getWidth() const {
		return m_width;
	}

	/**
	 * @brief Get the height of the maze
	 *
	 */
	int getHeight() const {
		return m_height;
	}

	/**
	 * @brief Return the index of a cell.
	 */
	int getIndex(int x, int y) const {
		return x + y * m_width;
	}

	/**
	 * @brief Return the coordinates of the cell.
	 */
	void getCoordinates(int cell, int& x, int& y) const {
		x = cell % m_width;
		y = cell / m_width;
	}

	/**
	 * @brief Check that the given cell is inside the maze.
	 */
	bool isCellInside(int x, int y) const {
		return x >= 0 && x < m_width && y >= 0 && y < m_height;
	}

	/**
	 * @brief Return the index of the neighbor cell of a maze and -1 if there is none.
	 */
	int getNeighborCell(int cell, int direction) const;

	/**
	 * @brief Checks whether a cell has a wall up at the given direction.
	 */
	virtual bool hasWall(int x, int y, int direction) const = 0;
	virtual bool hasWall(int c, int direction) const = 0;

	/**
	 * @brief Add or remove a wall in the cell toward the given direction.
	 */
	virtual void setWall(int x, int y, int direction, bool set = true) = 0;
	virtual void setWall(int cell, int direction, bool set = true) = 0;

	/**
	 * @brief Get the cells of the maze.
	 */
	virtual std::vector<Cell> getCells() const = 0;

	/**
	 * @brief If the maze is perfect, generate the corresponding tree.
	 */
	std::unique_ptr<Tree<Couple>> generateTree(int xRoot, int yRoot, int direction = -1) const;

	/**
	 * @brief Get the number of open walls.
	 */
	int countOpenWalls(void) const;

	/**
	 * @brief Print the maze.
	 */
	void print(std::ostream& stream) const;

protected:
	/**
	 * @brief The dimensions of the maze.S
	 */
	int m_width, m_height;
};

inline std::ostream& operator<<(std::ostream& stream, const Couple& couple) {
	return stream << '(' << couple.x << ',' << couple.y << ')';
}

inline bool operator!=(const Couple& c1, const Couple& c2) {
	return c1.x != c2.x && c1.y != c2.y;
}

inline std::ostream& operator<<(std::ostream& stream, const Maze& maze) {
	maze.print(stream);
	return stream;
}

#endif /* INCLUDE_MAZE_HPP_*/

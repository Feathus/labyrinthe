/*
 * Direction.hpp
 *
 *  Created on: Jun 15, 2015
 *      Author: laefy
 */

#ifndef DIRECTION_HPP_
#define DIRECTION_HPP_

#include <iostream>

class Direction {
public:
	/**
	 * @brief The name of the directions.
	 */
	enum Name {
		LEFT, UP, RIGHT, DOWN, COUNT
	};

	/**
	 * @brief Return the opposite direction.
	 */
	static int getOpposed(int direction);

	/**
	 * @brief Update x and y in order for them to contain the value of the neighbor pointed by direction.
	 */
	static void getNeighborCell(int& x, int& y, int direction);

	/**
	 * @brief Return the rotation of the direction by a 90° angle.
	 */
	static int getRotation90(int direction);

	/**
	 * @brief Return the rotation of the direction by a 270° angle.
	 */
	static int getRotation270(int direction);

	/**
	 * @brief Prints the direction in a stream.
	 */
	static void print(std::ostream &stream, int direction);
};

#endif /* DIRECTION_HPP_ */

/*
 * HardnessRater.hpp
 *
 *  Created on: 20 juin 2015
 *      Author: eric
 */

#ifndef INCLUDE_HARDNESSRATER_HPP_
#define INCLUDE_HARDNESSRATER_HPP_

class Maze;

class HardnessRater {
public:
	/**
	 * @brief Evaluates the difficulty of a maze.
	 * @param maze
	 * @return The difficulty of a maze.
	 */
	static int evaluate(const Maze &maze);
};

#endif /* INCLUDE_HARDNESSRATER_HPP_ */

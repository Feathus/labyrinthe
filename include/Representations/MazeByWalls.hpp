#ifndef GENERATOR_MAZEWALLREPRESENTATION_HPP_
#define GENERATOR_MAZEWALLREPRESENTATION_HPP_

#include "Maze.hpp"
#include <vector>

class MazeByWalls: public Maze {
public:
	/**
	 * @brief Constructor.
	 */
	MazeByWalls(int width, int height);

	/**
	 * @brief Destructor.
	 */
	virtual ~MazeByWalls();

	/**
	 * @brief Returns true if the room (x,y) has a wall in the direction given (0->left, 1->up, 2->right, 3->down).
	 */
	bool hasWall(int x, int y, int direction) const;
	bool hasWall(int c, int direction) const;

	/**
	 * @brief Add or remove a wall in the cell toward the given direction.
	 */
	void setWall(int x, int y, int direction, bool set = true);
	void setWall(int cell, int direction, bool set = true);

	/**
	 * @brief Creates a representation of the maze by cells.
	 */
	std::vector<Cell> getCells() const;

private:

	/**
	 * @brief Total number of inner walls and horizontal walls;
	 */
	int m_nbHorizontalWalls, m_nbWalls;

	/**
	 * @brief States for each wall if it is up or down.
	 *	Indexing of the walls: 		 ___ ___ ___ ___
	 *								|   8   11  14  |
	 *								|_0_|_1_|_2_|_3_|
	 *								|   9   12  15  |
	 *								|_4_|_5_|_6_|_7_|
	 *								|   10  13  16  |
	 *								|___|___|___|___|
	 */
	std::vector<bool> m_walls;

	/**
	 * @brief Gets the wall oriented at direction of the cell.
	 */
	int getWallIndex(int cell, int direction) const;

};

#endif /* INCLUDE_GENERATOR_MAZEWALLREPRESENTATION_HPP_ */

/*
 * Maze_cellRepresentation.hpp
 *
 *  Created on: 9 mai 2015
 *      Author: eric
 */

#ifndef INCLUDE_MAZE_BYCELLS_HPP_
#define INCLUDE_MAZE_BYCELLS_HPP_

#include "Maze.hpp"
#include <iostream>

class MazeByCells: public Maze {
public:
	/**
	 * @brief Constructor.
	 */
	MazeByCells(int width, int height);

	/**
	 * @brief Destructor.
	 */
	~MazeByCells();

	/**
	 * @ Override of hasWall.
	 */
	bool hasWall(int x, int y, int direction) const;
	bool hasWall(int c, int direction) const;

	/**
	 * @brief Add or remove a wall in the cell toward the given direction.
	 */
	void setWall(int x, int y, int direction, bool set = true);
	void setWall(int cell, int direction, bool set = true);

	/**
	 * @brief Gets the array of cells of the maze.
	 */
	std::vector<Cell> getCells(void) const;


private:
	/**
	 * @brief Cells of the maze.
	 */
	std::vector<Cell> m_cells;
};

#endif /* INCLUDE_MAZE_BYCELLS_HPP_ */

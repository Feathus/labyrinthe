#ifndef PROPERTYCHECKER_PROPERTYCHECKER_HPP_
#define PROPERTYCHECKER_PROPERTYCHECKER_HPP_

class Maze;

class PropertyChecker {
public:
	enum Similarity {
		HorizontalSymmetry = 1, VerticalSymmetry = 2, Rotation90 = 4, Rotation180 = 8, Rotation270 = 16,
		SameDimensions = 11, InvertedDimensions = 20
	};

	/**
	 * @brief Check whether the maze is perfect or not.
	 */
	static bool isPerfectMaze(const Maze& maze);

	/**
	 * @brief Return as a bit mask the transformation that can be applied to maze1 to obtain maze2.
	 */
	static int areSimilarMazes(const Maze& maze1, const Maze& maze2);
};

#endif /* PROPERTYCHECKER_PROPERTYCHECKER_HPP_ */

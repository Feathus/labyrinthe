#ifndef MESH_HPP
#define MESH_HPP

#include <GL/glew.h>

namespace _3DVisualizer {

/**
 * @brief The representation of a vertex (its position and its normal).
 */
typedef struct _Vertex {
	float position[3];
	float normal[3];
} Vertex;


class Mesh {
public:
	/**
	 * @brief Creates the vertex {position, normal}.
	 */
	static void createVertex(Vertex& vertex, float const position[3], float const normal[3]);

	/**
	 * @brief Creates the face (v1, v2, v3) in face.
	 */
	static void createFace(Vertex face[3], const Vertex& v1, const Vertex& v2, const Vertex& v3);
	static void createFace(Vertex face[3], float const pos1[3], float const norm1[3], float const pos2[3],
			float const norm2[3], float const pos3[3], float const norm3[3]);

	/**
	 * @brief Constructor.
	 */
	Mesh(int nbVertices);

	/**
	 * @brief Destructor.
	 */
	virtual ~Mesh();

	/**
	 * @brief Returns the number of vertices inside the mesh.
	 */
	inline int getNbVertices() const {
		return m_nbVertices;
	}

	/**
	 * @brief Returns the vbo identifier.
	 */
	inline GLuint getVBO() const {
		return m_vbo;
	}

protected:
	/**
	 * @brief Generates the associated vbo.
	 */
	void generateVBO(Vertex const *vertices);

	/**
	 * @brief The number of vertices inside this mesh.
	 */
	int const m_nbVertices;

private:
	/**
	 * @brief The identifier of the vbo.
	 */
	GLuint m_vbo;
};

}

#endif /* MESH_HPP */

#ifndef CUBE_HPP
#define CUBE_HPP

#include "3DVisualizer/Mesh.hpp"

namespace _3DVisualizer {

class Cube: public Mesh {
public:
	/**
	 * @brief Constructor.
	 */
	Cube();

private:
	/**
	 * @brief Constructs the vertices of the cube.
	 **/
	void construct(Vertex *vertices) const;
};

}

#endif /* CUBE_HPP_ */

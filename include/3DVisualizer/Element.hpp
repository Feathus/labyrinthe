#ifndef ELEMENT_HPP_
#define ELEMENT_HPP_

#include <GL/glew.h>

namespace _3DVisualizer {

class Cube;

class Element {
public:
	/**
	 * @brief Constructors.
	 */
	Element(const Cube& cube, float red, float green, float blue);

	/**
	 * @brief Destructor.
	 */
	virtual ~Element();

	/**
	 * @brief Draws the element.
	 */
	void draw() const;

private:
	/**
	 * @brief Generates the vbo corresponding to the colors.
	 */
	void generateColorVBO(float *colors, int size);

	/**
	 * @brief Generates the vao for this element.
	 */
	void generateVAO(int nbColorComponents);

	/**
	 * @brief The mesh used to design this element.
	 */
	const Cube& m_cube;

	/**
	 * @brief The identifier of the vbo containing the colors.
	 */
	GLuint m_colorVbo;

	/**
	 * @brief The identifier of the vao.
	 */
	GLuint m_vao;
};

} /* namespace _3DVisualizer */

#endif /* ELEMENT_HPP_ */

#ifndef SCENE_HPP_
#define SCENE_HPP_

#include "3DVisualizer/Shader.hpp"
#include "3DVisualizer/Camera.hpp"
#include "3DVisualizer/Element.hpp"
#include "3DVisualizer/Wall.hpp"
#include "3DVisualizer/Cube.hpp"
#include <vector>
#include <glm/glm.hpp>


class Maze;

namespace _3DVisualizer {

class Scene {
public:
	/**
	 * @brief Initializes GLEW (has to be done before creating any scene).
	 */
	static bool initializeOpenGL();

	/**
	 * @brief Constructor.
	 */
	Scene(double windowRatio, const Maze& maze);

	/**
	 * @brief Destructor.
	 */
	virtual ~Scene();

	/**
	 * @brief Initializes the scene.
	 * @return false if the initialization has failed.
	 */
	bool initialize();

	/**
	 * @brief Draw the elements of the scene on the GL buffer.
	 */
	void draw();

	/**
	 * @brief Methods for camera control.
	 */
	void rotateCamera(float dtheta, float dphi);
	void zoomCamera(float length);
	void shiftCamera(float length);

private:

	/**
	 * @brief The shaders used in the scene.
	 */
	Shader m_shader;

	/**
	 * @brief The camera of the scene.
	 */
	Camera m_camera;

	/**
	 * @brief The cube mesh.
	 */
	Cube m_cube;

	/**
	 * @brief The wall.
	 */
	Wall m_wall;

	/**
	 * @brief The positions of each wall in the scene.
	 */
	std::vector<Wall::Properties> m_wallProperties;

	/**
	 * @brief The floor of the scene.
	 */
	Element m_floor;

	/**
	 * @brief Dimensions of the maze.
	 */
	int m_width, m_height;
};

} /* namespace _3DVisualizer */

#endif /* SCENE_HPP_ */

#ifndef WALL_HPP_
#define WALL_HPP_

#include "3DVisualizer/Element.hpp"
#include <glm/glm.hpp>

namespace _3DVisualizer {

class Wall: Element {
public:
	/**
	 * @brief Define the orientation of a wall.
	 */
	enum class Orientation {
		HORIZONTAL, VERTICAL
	};

	/**
	 * @brief Define the position of a wall and its orientation.
	 */
	class Properties {
	friend class Wall;
	public:
		/**
		 * @brief Constructor.
		 */
		Properties(const glm::vec3& position, Orientation orientation)
		:	m_position(position), m_orientation(orientation) {}

	private:
		/**
		 * @brief The position of the wall.
		 */
		glm::vec3 m_position;

		/**
		 * @brief The orientation of the wall.
		 */
		Orientation m_orientation;
	};

	/**
	 * @brief Constructors.
	 */
	Wall(const Cube& cube);

	/**
	 * @brief Destructor.
	 */
	virtual ~Wall();

	/**
	 * @brief Draws the element.
	 */
	void draw(int modelLocation, const Properties& properties) const;

private:
	/**
	 * @brief Scales of the wall depending on their orientation.
	 */
	const static glm::vec3 horizontalScale, verticalScale;
};

} /* namespace _3DVisualizer */

#endif /* WALL_HPP_ */

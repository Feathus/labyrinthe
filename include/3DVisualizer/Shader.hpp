#ifndef SHADER_HPP_
#define SHADER_HPP_

#include <string>
#include <GL/glew.h>

namespace _3DVisualizer {

class Shader {
public:
	/**
	 * @brief Constructor.
	 */
	Shader(const std::string &vertexShader, const std::string &fragmentShader);

	/**
	 * @brief Destructor.
	 */
	virtual ~Shader();

	/**
	 * @brief Compiles the shader and creates the associated program.
	 * @return false if one of the action fails.
	 */
	bool load();

	/**
	 * @brief Binds or unbinds the shader program.
	 */
	void bind(bool bind = true) const;

	/**
	 * @brief Returns the location of the uniform variable "projection".
	 */
	inline GLint getProjectionLocation() {
		return m_projection;
	}

	/**
	 * @brief Returns the location of the uniform variable "view".
	 */
	inline GLint getViewLocation() const {
		return m_view;
	}

	/**
	 * @brief Returns the location of the uniform variable "model".
	 */
	inline GLint getModelLocation() const {
		return m_model;
	}

private:
	/**
	 * @brief Name (and location) of the vertex shader.
	 */
	std::string m_vertexPath;

	/**
	 * @brief Name (and location) of the fragment shader.
	 */
	std::string m_fragmentPath;

	/**
	 * @brief Identifier of the program.
	 */
	GLuint m_programID;

	/**
	 * @brief The location of the uniform variables "projection", "view" and "model".
	 */
	GLint m_projection;
	GLint m_view;
	GLint m_model;
};

} /* namespace _3DVisualizer */

#endif /* SHADER_HPP_ */

#ifndef WINDOW_HPP
#define WINDOW_HPP

	#include <string>

namespace sf {
class Window;
class Event;
}

namespace _3DVisualizer {

class Scene;

class Window {
public:
    /**
     * @brief Constructor.
     */
    Window(const std::string &name, int width, int height);

    /**
	 * @brief Destructor.
	 */
	~Window();

	/**
	 * @brief Return whether the window has been closed or not.
	 */
	bool isOpen();

    /**
     * @brief Renders the scene.
     */
    void renderScene(Scene& scene);

    /**
     * @brief Polls an event from the window.
     */
    bool pollEvent(Scene& scene);

    /**
     * @brief Closes the window.
     */
    void close();

    /**
     * @brief Returns the result of width / height.
     */
    double getRatio();

private:
    /**
     * @brief The window.
     */
    sf::Window *m_window;
};

} /* namespace 3DVisualizer */

#endif // WINDOW_HPP


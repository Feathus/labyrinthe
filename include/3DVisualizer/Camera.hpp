#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <glm/glm.hpp>

namespace _3DVisualizer {

class Camera {
public:
	/**
	 * @brief Constructor.
	 */
	Camera(glm::vec3 position, glm::vec3 target, double angle, double ratio, double near, double far);

	/**
	 * @brief Destructor.
	 */
	virtual ~Camera();

	/**
	 * @brief Sets a new position for the camera.
	 */
	void changePosition(glm::vec3 position);

	/**
	 * @brief Changes the target point.
	 */
	void changeTarget(glm::vec3 target);

	/**
	 * @brief Moves the camera.
	 */
	void move(float theta, float phi);

	/**
	 * @brief Changes the orientation of the camera.
	 */
	void reorientate(float theta, float phi);

	/**
	 * @brief Zooms in or out.
	 */
	void zoom(float length);

	/**
	 * @brief Shifts the camera.
	 */
	void shift(float length);

	/**
	 * @brief Returns the projection matrix.
	 */
	inline const glm::mat4& getProjection() const {
		return m_projection;
	}

	/**
	 * @brief Returns the view matrix.
	 */
	const glm::mat4& getView();

private:
	/**
	 * @brief The position of the camera.
	 */
	glm::vec3 m_position;

	/**
	 * @brief The location the camera is looking at.
	 */
	glm::vec3 m_target;

	/**
	 * @brief The projection matrix.
	 */
	glm::mat4 m_projection;

	/**
	 * @brief The view matrix.
	 */
	glm::mat4 m_view;

	/**
	 * @brief Indicates whether the camera position or its orientation has changed.
	 */
	bool m_hasChanged;

};

} /* namespace _3DVisualizer */

#endif /* CAMERA_HPP_ */

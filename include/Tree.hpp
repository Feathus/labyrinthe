#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <utility>

/**
 * @brief Representation of a tree.
 */
template <typename T>
class Tree {

public:

	/** 
	 * @brief Constructor.
	 */
	Tree(const T& content) 
	:	m_content(content), m_isStrahlerEvaluate(false) {
	}

	/**
	 * @brief Destructor.
	 */
	virtual ~Tree() {
	}

	/**
	 * @brief Adds a child to the tree if not NULL.
	 */

	std::unique_ptr<Tree<T>>& addChild(std::unique_ptr<Tree<T>> child) {
		if (child) {
			m_childs.push_back(std::move(child));
		}
		return m_childs.back();
	}

	/**
	 * @brief Prints the content of the tree in stream.
	 */
	void print(std::ostream& stream) const {
		print(stream, 0);
	}

	/**
	 * @brief Gets the strahler number of the tree.
	 */
	int getStrahlerNumber(void)  {
		if (!m_isStrahlerEvaluate) {
			return evaluateStrahler();
		}
		return m_strahler;
	}

private:
	
	/**
	 * @brief Content of the node.
	 */
	T m_content;

	/**
	 * @brief Children of the node.
	 */
	std::vector<std::unique_ptr<Tree<T>>> m_childs;

	/**
	 * @brief Strahler number of the node.
	 */
	int m_strahler = 0;

	/**
	 * @brief Checks whether the strahler number is evaluate or not.
	 */
	bool m_isStrahlerEvaluate;

	/**
	 * @brief Evaluates the Strahler number of each node of the tree.
	 */
	int evaluateStrahler(void) {
		for (int i = 0; i < nbChildren() ; i ++) {
			m_childs[i]->evaluateStrahler();
			evaluateStrahlerNode();
		}
		m_isStrahlerEvaluate = true;
		return m_strahler;
	}

	/**
	 * @brief Evaluates the Strahler number of a node.
	 */
	void evaluateStrahlerNode(void) {
		if (isLeaf()) {
			m_strahler = 1;
			return;
		}
		int max = maxStrahlerChildren();
		int nbChildsEqMax = strahlerNumberChildrenEqual(max);
		int nbChildsLessMax = strahlerNumberChildrenLess(max);
		int nbChilds = nbChildren();
		if (nbChildsEqMax == 1 && nbChildsLessMax == nbChilds - 1) {
			m_strahler = max;
		}
		else if (nbChildsEqMax >= 2 && nbChildsEqMax + nbChildsLessMax == nbChilds) {
			m_strahler = max + 1;
		}
	}

	/**
	 * @brief Checks whether the current node is a leaf.
	 */
	bool isLeaf(void) {
		return m_childs.empty();
	}

	/**
	 * @brief Gets the number of children of a node.
	 */
	int nbChildren(void) {
		return m_childs.size();
	}

	/**
	 * @brief Returns the number of children where the Strahler number is equal to valueMatch. 
	 */
	int strahlerNumberChildrenEqual(int valueMatch) const {
		int c = 0;
		for (const auto& child: m_childs) {
			if (child->m_strahler == valueMatch) {
				++ c;
			}
		}
		return c;
	}

	/**
	 * @brief Returns the number of children where the Strahler number is less than valueMatch. 
	 */
	int strahlerNumberChildrenLess(int valueMatch) const {
		int c = 0;
		for (const auto& child: m_childs) {
			if (child->m_strahler < valueMatch) {
				c++;
			}
		}
		return c;
	}

	/**
	 * @brief Returns the maximum Strahler number of children. 
	 */
	int maxStrahlerChildren(void) const {
		int max = m_childs[0]->m_strahler;
		for (const auto& child: m_childs) {
			if (max < child->m_strahler) {
				max = child->m_strahler;
			}
		}
		return max;
	}

	/**
	 * @brief Prints the tree recursively on the stream.
	 */
	void print(std::ostream& stream, int level) const {
		for (auto i = 0; i < level; i ++) {
			stream << "  ";
		}
		stream << "-> " << m_content << std::endl;
		for (const auto& child: m_childs) {
			child->print(stream, level + 1);
		}
	}
};

/**
 * @brief Overloading of << for Tree<T> objects.
 */
template <typename T>
std::ostream& operator<<(std::ostream& stream, Tree<T> tree) {
	tree.print(stream);
	return stream;
}

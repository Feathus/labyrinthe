#pragma once

#include "Solver/MazeSolver.hpp"
#include "Solver/MazeSolution.hpp"
#include "Maze.hpp"
#include <vector>
#include <SFML/Graphics.hpp>

/**
 * @brief Class which creates a window and draws a maze.
 */
class Maze_2dVisualizer {
public:
	/**
	 * @brief Constructor.
	 */
	Maze_2dVisualizer(const Maze& maze);
	Maze_2dVisualizer(const Maze& maze, const MazeSolution& solution);

	/**
	 * @brief Destructor.
	 */
	~Maze_2dVisualizer();

	/**
	 * @brief Main loop event.
	 */
	void mainLoop(void);


private:
	/**
	 * @brief Initialize the window.
	 */
	void initializeWindow();

	/**
	 * @brief The maze to display.
	 */
	const Maze *m_maze;

	/**
	 * @brief The solution of the maze.
	 */
	const MazeSolution *m_solution;

	/**
	 * @brief Dimensions of the window.
	 */
	int m_widthWindow, m_heightWindow;

	/**
	 * @brief Border size of the window.
	 */
	const int m_border = 15;

	/** 
	 * @brief Size of a cell (width = height).
	 */
	int m_sizeCell;

	/**
	 * @brief Buttons zone width.
	 */
	const int m_wZButtons = 100;

	/**
	 * @brief Zone of the maze in the window.
	 */
	sf::IntRect m_zoneMaze;

	/**
	 * @brief Color of the Maze.
	 */
	sf::Color m_colorMaze = sf::Color::Black;
	sf::Color m_bgColor = sf::Color::White;

	/**
	 * @brief The window.
	 */
	sf::RenderWindow m_window;

	/**
	 * @brief View of the maze.
	 */
	sf::View m_view;

	/**
	 * @brief Gets an ideal size of the window depending the size of the maze.
	 */
	void getIdealDimensions(void);

	/**
	 * @brief Displays a vertical wall (a line).
	 */
	void displayVerticalWall(int x, int y);

	/**
	 * @brief Displays a horizontal wall (a line).
	 */
	void displayHorizontalWall(int x, int y);

	/**
	 * @brief Displays an indicator that state whether the cell is inside the solution.
	 */
	void displaySolutionCell(int x, int y);

	/**
	 * @brief Sets the 2D Camera of the maze.
	 */
	void setView();

	/**
	 * @brief Camera zooms in the maze.
	 */
	void zoomIn(void);

	/**
	 * @brief Camera zooms out the maze.
	 */
	void zoomOut(void);

	/**
	 * @brief Camera moves left.
	 */
	void moveLeft(void);

	/**
	 * @brief Camera moves right.
	 */
	void moveRight(void);

	/**
	 * @brief Camera moves top.
	 */
	void moveUp(void);

	/**
	 * @brief Camera moves bottom.
	 */
	void moveDown(void);

	/**
	 * @brief Displays the solution of the maze.
	 */
	void displaySolution(void);

	/**
	 * @brief Displays the entire maze.
	 */
	void displayMaze(void);

};

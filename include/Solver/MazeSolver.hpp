#ifndef SOLVER_MAZESOLVER_HPP_
#define SOLVER_MAZESOLVER_HPP_

#include "Solver/MazeSolution.hpp"
#include <list>

class Maze;

class MazeSolver {
public:
	/**
	 * @brief Constructor.
	 */
	MazeSolver();

	/**
	 * @brief Destructor.
	 */
	virtual ~MazeSolver();

    /**
     * @brief Solve the maze.
     * @return The solution.
     */
	virtual MazeSolution solve(const Maze& maze, int entrance = 0, int exit = -1) = 0;

	/**
	 * @brief Return whether a solution has been found.
	 */
	bool hasSolved();

protected:
	/**
	 * @brief Initialize the solver.
	 */
	void initialize(const Maze& maze, int entrance, int exit);

	/**
	 * @brief The maze which solution is currently being calculated.
	 */
	Maze const *m_maze;

	/**
	 * @brief The entrance and exit cells of the maze.
	 */
	int m_entrance, m_exit;

	/**
	 * @brief State whether the last generation went well.
	 */
	bool m_status;
};

#endif

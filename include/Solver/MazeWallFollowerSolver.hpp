#ifndef SOLVER_MAZEWALLFOLLOWERSOLVER_HPP
#define	SOLVER_MAZEWALLFOLLOWERSOLVER_HPP

#include "Solver/MazeSolver.hpp"
#include "Maze.hpp"


class MazeWallFollowerSolver: public MazeSolver {
public:
    /**
     * @brief Constructor.
     */
    MazeWallFollowerSolver();
    
    /**
     * @brief Destructor.
     */
    virtual ~MazeWallFollowerSolver();
    
    /**
     * @brief Solve the maze and return the solution.
     * @return The solution of the maze.
     */
	MazeSolution solve(const Maze& maze, int entrance = 0, int exit = -1);

private:
	/**
	 * @brief The representation of a cell that can be visited twice.
	 */
	class Visit {
	public:
		Visit()
		:	m_visited(0), m_direction(-1)
		{
		}

		virtual ~Visit() {
		}

		int m_visited;
		int m_direction;
	};
	/**
	 * @brief Find a solution for the maze.
	 */
    MazeSolution followWalls();
};

#endif


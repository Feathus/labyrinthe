#ifndef SOLVER_MAZESOLUTION_HPP_
#define SOLVER_MAZESOLUTION_HPP_

#include "Maze.hpp"
#include <list>

/**
 * @brief Class used to represent a solution of the maze.
 */
class MazeSolution: public std::list<int> {

public:
	/**
	 * @brief Constructor.
	 */
	MazeSolution()
	:	std::list<int>(), m_entrance(0)
	{
	}

	MazeSolution(int entrance)
	:	std::list<int>(), m_entrance(entrance)
	{
	}

	/**
	 * @brief Destructor.
	 */
	virtual ~MazeSolution() {
	}

    int getEntrance(void) const {
    	return m_entrance;
    }

private:
	/**
	 * @brief The entrance cell of the maze.
	 */
	int m_entrance;
};

#endif

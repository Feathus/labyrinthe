#ifndef SOLVER_MAZEBACKTRACKSOLVER_HPP
#define	SOLVER_MAZEBACKTRACKSOLVER_HPP

#include "Solver/MazeSolver.hpp"

class MazeBacktrackSolver: public MazeSolver {
public:
    /**
     * @brief Constructor.
     */
    MazeBacktrackSolver();
    
    /**
     * @brief Destructor.
     */
    virtual ~MazeBacktrackSolver();
    
    /**
     * @brief Solve the maze.
     * @return The solution of the maze.
     */
	MazeSolution solve(const Maze& maze, int entrance = 0, int exit = -1);
    
private:
	/**
	 * @brief The representation of a cell that can be visited once.
	 */
	class Visit {
	public:
		Visit()
		:	m_visited(false), m_direction(-1)
		{
		}

		virtual ~Visit() {
		}

		bool m_visited;
		int m_direction;
	};

    /**
     * @brief Solve the maze using the iterative backtracker algorithm (no stack overflow).
     */
    MazeSolution generateSolution();
};

#endif	/* MAZEBACKTRACKSOLVER_HPP */


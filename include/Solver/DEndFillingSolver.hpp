/*
 * DEndFillingSolver.hpp
 *
 *  Created on: 21 juin 2015
 *      Author: eric
 */

#ifndef INCLUDE_SOLVER_DENDFILLINGSOLVER_HPP_
#define INCLUDE_SOLVER_DENDFILLINGSOLVER_HPP_

#include "Solver/MazeSolver.hpp"

class DEndFillingSolver: public MazeSolver {
public:

	/**
	 * @brief Constructor.
	 */
	DEndFillingSolver();

	/**
	 * @brief Destructor.
	 */
	virtual ~DEndFillingSolver();

	/**
	 * @brief Solve the maze using Dead-End Filling algorithm.
	 */
	MazeSolution solve(const Maze& maze, int entrance = 0, int exit = -1);

	/**
	 * @brief Get the number of cells which leads to a dead-end.
	 * @return
	 */
	int getDeadEndCells(void) const;

	/**
	 * @brief Get the number of dead-ends in the maze.
	 */
	int getDeadEnds(void) const;

private:

	/**
	 * @brief Solutions of the maze.
	 */
	std::vector<MazeSolution> m_solutions;

	/**
	 * @brief Representation of a cell.
	 */
	struct Cell {
		bool m_visited;
		bool m_condamned;
	};

	/**
	 * @brief Cells of the maze.
	 */
	std::vector<Cell> m_visited;

	/**
	 * @brief Number of cells which leads to a dead-end.
	 */
	int m_nbDeadEndCells;

	/**
	 * @brief Number of dead-ends.
	 */
	int m_nbDeadEnds;

	/**
	 * @brief Number of cells which are in an intersection.
	 * nbIntersections = nbCells - nbDeadEndCells.
	 */
	int m_nbIntersectionCells;

	/**
	 * @brief Check whether a cell leads to an intersection.
	 * @param cell
	 * @return true / false
	 */
	bool isIntersection(int cell);

	/**
	 * @brief Check whether a cell leads to a dead-end.
	 * @param cell
	 * @return true / false
	 */
	bool isDeadEnd(int cell);

	/**
	 * @brief Get the accessible neighbors.
	 * @param cell
	 * @return The neighbors
	 */
	int getAccessibleNeighbors(int cell);

	/**
	 * @brief Check whether a neighbor is accessible at a direction.
	 * @param neighbors
	 * @param direction
	 * @return true / false
	 */
	bool isAccessibleNeighbor(int neighbors, int direction) ;

	/**
	 * @brief Choose the direction
	 * @param neighbors
	 * @return
	 */
	int chooseDirection(int neighbors);

	/**
	 * @brief Generate all the solutions of the maze.
	 * @return false if there are no solution avaible, true otherwise.
	 */
	bool generateSolutions(void);

	/**
	 * @brief Return the best solution of all the solutions generated.
	 * @return
	 */
	MazeSolution generateBestSolution(void);

};

#endif /* INCLUDE_SOLVER_DENDFILLINGSOLVER_HPP_ */

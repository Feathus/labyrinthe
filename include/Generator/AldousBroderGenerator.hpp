#ifndef GENERATOR_ALDOUSBRODERGENERATOR_HPP_
#define GENERATOR_ALDOUSBRODERGENERATOR_HPP_

#include "MazeGenerator.hpp"
#include <vector>

class Maze;

class AldousBroderGenerator: public MazeGenerator {
public:
	/**
	 * @brief Constructor.
	 */
	AldousBroderGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~AldousBroderGenerator();

	/**
	 * @brief Generates the maze using Aldous-Broder algorithm.
	 */
	void generate(Maze& maze);

private:

	/**
	 * @brief Checks whether a cell has a wall at a direction.
	 * @param cell
	 * @param direction
	 * @return true / false
	 */
	bool hasWall(unsigned char cell, int direction);

	/**
	 * @brief Breaks a wall at the direction.
	 * @param cell
	 * @param direction
	 */
	void breakWall(unsigned char *cell, int direction);

	/**
	 * Marks a cell visited.
	 * @param cell
	 */
	void setVisited(unsigned char *cell);

	/**
	 * @brief Checks whether a cell has been visited.
	 * @param cell
	 * @return true/false
	 */
	bool isVisited(unsigned char cell);
};

#endif

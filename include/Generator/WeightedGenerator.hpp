#ifndef GENERATOR_WEIGHTEDGENERATOR_HPP_
#define GENERATOR_WEIGHTEDGENERATOR_HPP_

#include "Generator/KruskalGenerator.hpp"

class WeightedGenerator: public KruskalGenerator {
public:
	/**
	 * @brief Constructor.
	 */
	WeightedGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~WeightedGenerator();

	/**
	 * @brief Generate the maze.
	 */
	void generate(Maze& maze);
};

#endif

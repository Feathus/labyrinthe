#ifndef GENERATOR_LOOPERGENERATOR_HPP_
#define GENERATOR_LOOPERGENERATOR_HPP_

#include "Generator/MazeGenerator.hpp"
#include <list>

class LooperGenerator: public MazeGenerator {
public:
	/**
	 * @brief Constructor.
	 */
	LooperGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~LooperGenerator();

	/**
	 * @brief Generate the maze.
	 */
	void generate(Maze& maze);

private:
	/**
	 * @brief Give the number of connected walls for each extremity of the gate.
	 */
	class ConnectionCount {
		friend class LooperGenerator;

	public:
		ConnectionCount()
		: m_count1(3), m_count2(3)
		{
		}

		virtual ~ConnectionCount() {
		}

	private:
		int m_count1, m_count2;
	};

	/**
	 * @brief Contain the number of the connected wall, and on which side of it we are connected.
	 */
	class Connection {
		friend class LooperGenerator;

	public:
		Connection(int wall, bool side)
		: m_wall(wall), m_side(side)
		{
		}

		virtual ~Connection() {
		}

	private:
		int m_wall;
		bool m_side;
	};


	/**
	 * @brief Return the number of borders the wall is connected to.
	 */
	void updateBorderCount(int wall, std::vector<ConnectionCount>& counts);

	/**
	 * @brief Put in connected the walls that are connected to the given one.
	 */
	void getConnectedWalls(int wall, std::list<Connection>& connected, int c1, int c2);

	/**
	 * @brief Break the given wall, and update the number of adjacent walls for each connected wall.
	 */
	void breakWall(int wall, std::vector<ConnectionCount>& counts, const std::list<Connection>& connected);
};

#endif /* GENERATOR_LOOPERGENERATOR_HPP_ */

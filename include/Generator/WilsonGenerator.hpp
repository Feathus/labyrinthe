/*
 * WilsonGenerator.hpp
 *
 *  Created on: 19 juin 2015
 *      Author: eric
 */

#ifndef INCLUDE_GENERATOR_WILSONGENERATOR_HPP_
#define INCLUDE_GENERATOR_WILSONGENERATOR_HPP_

#include "Generator/MazeGenerator.hpp"
#include "Maze.hpp"
#include <vector>
#include <list>

class WilsonGenerator: public MazeGenerator {
public:
	/**
	 * @brief Constructor.
	 */
	WilsonGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~WilsonGenerator();

	/**
	 * @brief Generate the maze using Wilson algorithm.
	 * @param maze
	 */
	void generate(Maze &maze);

private:

	/**
	 * @brief Add a cell to the maze (the UST: Uniform Spanning Tree).
	 * @param remainingCells
	 * @param cell
	 */
	void addCellToUST(std::vector<bool>& remainingCells, int &nbRemainingCells, int cell);

	/**
	 * @brief Checks whether a cell is not already in the maze.
	 * @param remainingCells
	 * @param cell
	 * @return true / false
	 */
	bool isRemainingCell(const std::vector<bool> &remainingCells, int cell);

	/**
	 * @brief Perform a random walk and adds all cells visited to the maze.
	 * @param remainingCells
	 * @param startCell
	 */
	void randomWalk(std::vector<bool> &remainingCells, int &nbRemainingCells, int startCell, int startDirection);

	/**
	 * @brief Checks whether the wall at dir is not a border wall.
	 * @param cell
	 * @param dir
	 * @return true / false
	 */
	bool isValidDirection(int cell, int dir);

	/**
	 * @brief Select a random valid direction .
	 * @param cell
	 * @return The direction selected.
	 */
	int chooseRandomDirection(int cell);

	/**
	 * @brief Update the list of cells susceptible to pick .
	 * @param remainingCells
	 * @param cellsToPick
	 */
	void updateList(const std::vector<bool>& remainingCells, std::list<int>& cellsToPick);

};

#endif /* INCLUDE_GENERATOR_WILSONGENERATOR_HPP_ */

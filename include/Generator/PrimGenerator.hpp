#ifndef GENERATOR_PRIMGENERATOR_HPP_
#define GENERATOR_PRIMGENERATOR_HPP_

#include "MazeGenerator.hpp"
#include "Maze.hpp"
#include <list>


class PrimGenerator: public MazeGenerator {
public:
	/**
	 * @brief Constructor.
	 */
	PrimGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~PrimGenerator();

	/**
	 * @brief Generate the maze using Prim algorithm.
	 */
	void generate(Maze& maze);

private:
	/**
	 * @brief Adds the walls around a cell into the list of breakable walls.
	 */
	void addBreakableWalls(std::list<int>& breakables, int x, int y) const;

	/**
	 * @brief Extract a random wall stored into a list.
	 */
	int extractBreakableWall(std::list<int> &list) const;

	/**
	 * @brief Gets the wall oriented at direction of the cell.
	 */
	int getWallCell(int x, int y, int direction, int nbHorizontals) const;
};

#endif /* INCLUDE_GENERATOR_PRIMGENERATOR_HPP_ */

#pragma once

#include <random>
#include <vector>

class Maze;

class MazeGenerator {
public:
	/**
	 * @brief Constructor.
	 */
	MazeGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~MazeGenerator();

	/**
	 * @brief Generates a maze.
	 */
	virtual void generate(Maze &maze) = 0;

protected:
	/**
	 * @brief The random device.
	 */
	static std::mt19937 m_generator;

	/**
	 * @brief Initialize the generator to process the given maze.
	 */
	void initialize(Maze& maze, bool up = true);

	/**
	 * @brief Return the cells that are adjacent to the given wall.
	 */
	void getAdjacentCells(int wall, int& c1, int& c2) const;
	void getAdjacentCells(int wall, int& x1, int& y1, int& x2, int& y2) const;

	/**
	 * @brief Get the index of the wall corresponding to the given cell and direction.
	 */
	int getWall(int cell, int direction) const;
	int getWall(int x, int y, int direction) const;

	/**
	 * @brief Load all the walls into the maze.
	 */
	void updateMaze() const;

	/**
	 * @brief Pointer toward the maze that is currently being generated.
	 */
	Maze *m_maze;

	/**
	 * @brief State whether the walls of the currently generated maze or up or down.
	 */
	std::vector<bool> m_walls;
	int m_nbHorizontalWalls;
};

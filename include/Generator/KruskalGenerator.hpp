#ifndef GENERATOR_KRUSKALGENERATOR_HPP_
#define GENERATOR_KRUSKALGENERATOR_HPP_

#include "Maze.hpp"
#include "MazeGenerator.hpp"
#include <vector>
#include <list>
#include <algorithm>

class KruskalGenerator: public MazeGenerator {

public:
	/**
	 * @brief Constructor.
	 */
	KruskalGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~KruskalGenerator();

	/**
	 * @brief Kruskal generation.
	 */
	virtual void generate(Maze& maze);

protected:
	/**
	 * @brief Represent a group of cell that are mutually accessible.
	 */
	class CellGroup {
		friend class KruskalGenerator;

	public:
		CellGroup(int group)
		: m_group(group), m_cells()
		{
			m_cells.push_front(group);
		}

		virtual ~CellGroup() {
		}

	private:
		int m_group;
		std::list<int> m_cells;
	};

	/**
	 * @brief Gives the same values to the cells in the same zone as c1 and c2.
	 */
	void uniteCells(std::vector<int>& cells, int c1, int c2, std::list<CellGroup>& groupList, std::list<int>& breakables);

};

#endif /* INCLUDE_GENERATOR_KRUSKALGENERATOR_HPP_ */

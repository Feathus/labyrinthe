#ifndef GENERATOR_UNIFORMGENERATOR_HPP_
#define GENERATOR_UNIFORMGENERATOR_HPP_

#include "MazeGenerator.hpp"

class UniformGenerator: public MazeGenerator {
public:
	/**
	 * @brief Constructor.
	 */
	UniformGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~UniformGenerator();

	/**
	 * @brief Generate uniformly a perfect or non perfect maze.
	 */
	void generate(Maze& maze);
};

#endif

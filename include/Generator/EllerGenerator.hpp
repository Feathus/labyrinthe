/*
 * EllerGenerator.hpp
 *
 *  Created on: 19 juin 2015
 *      Author: eric
 */

#ifndef INCLUDE_GENERATOR_ELLERGENERATOR_HPP_
#define INCLUDE_GENERATOR_ELLERGENERATOR_HPP_


#include "Generator/MazeGenerator.hpp"
#include "Maze.hpp"
#include <list>
#include <algorithm>

class EllerGenerator: public MazeGenerator {
public:

	/**
	 * @brief Constructor.
	 */
	EllerGenerator();

	/**
	 * @brief Destructor.
	 */
	virtual ~EllerGenerator();

	/**
	 * @brief Generate the maze using Eller's algorithm.
	 * @param maze
	 */
	void generate(Maze &maze);

private:
	/**
	 * @brief Width of a line.
	 */
	int m_lineWidth;


	/**
	 * @struct Cell
	 * @brief Representation of a cell.
	 */
	struct Cell {
		int set;
		bool alone;
	};

	/**
	 * @struct CellSet
	 * @brief Representation of a set of cells.
	 */
	class CellSet {
		friend class EllerGenerator;
	public:
		CellSet(int id)
		: m_id(id), m_cells(), m_connect(false)
		{
			m_cells.push_back(id);
		}

		virtual ~CellSet() {}

		void addCell(int cell) {
			m_cells.push_back(cell);
		}

		bool containsCell(int cell) {
			return std::find(m_cells.cbegin(), m_cells.cend(), cell) != m_cells.cend();
		}

		bool operator==(const CellSet &c) {
			return m_id == c.m_id;
		}

	private:
		int m_id;
		std::list<int> m_cells;
		bool m_connect;
	};

	/**
	 * @brief List of sets.
	 */
	std::list<CellSet> m_cellSets;

	/**
	 * @brief Generate a line of the Maze.
	 */
	void generateLine(int nbLinesGenerated, std::vector<Cell>& cells);

	/**
	 * @brief Generate the last line of the maze.
	 * @param nbLinesGenerated
	 * @param cells
	 * @param cellSets
	 */
	void generateLastLine(std::vector<Cell>& cells);

	void initializeLine(int yCur);

	void horizontalConnections(int x, int yCur, std::vector<Cell> &cells);

	void verticalConnections(int x, int yCur, std::vector<Cell> &cells);

	void connectIsolatedCells(int limitCell, std::vector<Cell> &cells);

	void mergeSets(int set1, int set2, std::vector<Cell> &cells);

	std::list<CellSet>::iterator findSet(int idSet);

	bool sameSet(int cell, int dir, const std::vector<Cell>& cells) const;


	/**
	 * @brief Break the right wall of a cell.
	 * @param cell
	 */
	int breakVerticalWall(int cell) ;

	/**
	 * @brief Break the down wall of a cell.
	 * @param cell
	 */
	int breakHorizontalWall(int cell);

};

#endif /* INCLUDE_GENERATOR_ELLERGENERATOR_HPP_ */
